__version__ = '1.0'
__author__ = 'Filippo Baruffaldi <filippo@baruffaldi.info>'
__url__ = 'http://www.gbc-italy.com/'
__license__ = 'MIT'
__docformat__ = 'restructuredtext'

from gbkit.wsgi.handlers import RequestHandler as GBKitRequestHandler


class RequestHandler(GBKitRequestHandler):
    _requested_format = 'json'
    api_mode = True
    def set_development_environment(self):
        #self.forced_factories_output = True
        self.forced_user_output = True
