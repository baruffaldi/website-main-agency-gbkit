#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

from gbkit.wsgi import WSGIApplication
from gbkit.wsgi.handlers import RequestHandler, DEBUG, wsgi_config


class AdminTemplateViewer(RequestHandler):
    def get(self):
        self.render(
            self.get_template(self.request.get('tpl', default_value='')))


class AdminSettings(RequestHandler):
    def get(self):
        return
        self.native_render(
            self.path(
                self.gbkit_templates_path(),
                'admin', 'settings.html'))


class AdminHistory(RequestHandler):
    def get(self):
        return
        self.native_render(
            self.path(
                self.gbkit_templates_path(),
                'admin', 'history.html'))


class AdminProfile(RequestHandler):
    def get(self):
        return
        self.native_render(
            self.path(
                self.gbkit_templates_path(),
                'admin', 'profile.html'))


class AdminUsers(RequestHandler):
    def get(self):
        return
        self.native_render(
            self.path(
                self.gbkit_templates_path(),
                'admin', 'users.html'))


class Index(RequestHandler):
    def get(self, *args, **kwargs):
        #from pygments import highlight
        #from pygments.formatters import HtmlFormatter
        #from pygments.lexers.json import JSONLexer
        self.response.out.write("""<html><head>
<script type="text/javascript">
function changeColors(el, weight, color, borders, rounded){
    el.style.fontWeight = weight;
    el.style.backgroundColor = color;
    el.style.border = borders;
    el.style.borderRadius = rounded;
}
function focusMe(el){
    for (var i=0; i<=links.length; i++) {
        if (links[i])
        changeColors(links[i], 'normal', 'transparent', '0', '5px');
    }
    changeColors(el, 'bold', '#E5ECF9', '0', '5px')
}
var links = document.getElementsByTagName('a')
</script>
<style>
a{
color: #00C;
font-family: Arial, sans-serif;
font-size: 13px;
line-height: 19.5px;
min-width: 0px;
text-align: left;
text-decoration: underline;
width: auto;
padding: 3px 5px;margin-bottom: 5px;}
p{padding:0;margin:0;}
</style>
    </head><body>
            <!--<h1 style="margin:0;padding:3px;width:100%;background-color: #E5ECF9;">GBKit Tests</h1>-->
            <div style="padding:7px;border-bottom: 3px solid #E5ECF9;">
            <p style="">
            <a style="font-weight: bold; background-color: #E5ECF9; border-radius: 5px;" onclick="focusMe(this)" href="/gbkit/admin/settings" target="test_box">Settings</a> -
            <a onclick="focusMe(this)" href="/gbkit/admin/history" target="test_box">History</a> -
            <a onclick="focusMe(this)" href="/gbkit/admin/users" target="test_box">Users</a> -
            <a onclick="focusMe(this)" href="/gbkit/admin/profile" target="test_box">Profile</a>
            </p>
            </div>
            <iframe seamless="1" height="100%" width="100%" name="test_box" src="/gbkit/admin/settings"> </iframe>
            </body></html>""")


class NotFound(RequestHandler):
    def get(self):
        self.raise_error(code=404)


routes = [
    (r'/.*/admin/settings', AdminSettings),
    (r'/.*/admin/history', AdminHistory),
    (r'/.*/admin/users', AdminUsers),
    (r'/.*/admin/profile', AdminProfile),
    (r'/.*/admin/template-viewer', AdminTemplateViewer),

    (r'/.*/admin/index', Index),
    (r'/.*/admin/', Index),
    (r'.*', NotFound)
]
app = WSGIApplication(routes, debug=DEBUG, config=wsgi_config)
