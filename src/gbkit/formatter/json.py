#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import os
import simplejson

DEBUG = os.environ.get('SERVER_SOFTWARE').startswith("Dev")
if 'debug=1' in os.environ.get('QUERY_STRING', ''):
    DEBUG = True
if 'debug=0' in os.environ.get('QUERY_STRING', ''):
    DEBUG = False
import datetime
import time

from google.appengine.api import users
from google.appengine.ext import db


class GqlEncoder(simplejson.JSONEncoder):

    """Extends JSONEncoder to add support for GQL results and properties.

    Adds support to simplejson JSONEncoders for GQL results and properties by
    overriding JSONEncoder's default method.
    """

    # TODO Improve coverage for all of App Engine's Property types.

    def default(self, obj, _default=None):

        """Tests the input object, obj, to encode as JSON."""

        if hasattr(obj, '__json__'):
            return getattr(obj, '__json__')()

        if isinstance(obj, db.GqlQuery):
            return list(obj)

        elif isinstance(obj, db.Model):
            properties = obj.properties().items()
            if hasattr(obj, 'key') and obj.is_saved():
                output = {'key': str(obj.key())}
            else:
                output = {}
            for field, value in properties:
                if isinstance(getattr(obj, field), db.Model) and getattr(obj, field).is_saved():
                    output[field] = str(getattr(obj, field).key())
                else:
                    output[field] = getattr(obj, field)
            return output

        elif isinstance(obj, datetime.datetime):
            output = {}
            fields = ['day', 'hour', 'microsecond', 'minute', 'month', 'second', 'year']
            methods = ['ctime', 'isocalendar', 'isoformat', 'isoweekday', 'timetuple']
            for field in fields:
                output[field] = getattr(obj, field)
            for method in methods:
                output[method] = getattr(obj, method)()
            output['epoch'] = time.mktime(obj.timetuple())
            return output

        elif isinstance(obj, datetime.date):
            output = {}
            fields = ['year', 'month', 'day']
            methods = ['ctime', 'isocalendar', 'isoformat', 'isoweekday', 'timetuple']
            for field in fields:
                output[field] = getattr(obj, field)
            for method in methods:
                output[method] = getattr(obj, method)()
            output['epoch'] = time.mktime(obj.timetuple())
            return output

        elif isinstance(obj, time.struct_time):
            return list(obj)

        elif isinstance(obj, Exception):
            return obj.message

        elif getattr(obj, 'items', False):
            return obj.items()

        elif isinstance(obj, users.User):
            output = {}
            methods = ['nickname', 'email', 'auth_domain']
            for method in methods:
                output[method] = getattr(obj, method)()
            return output

        output = simplejson.JSONEncoder.default(self, obj)

        if output == 'null' and _default is not None:
            output = GqlEncoder(sort_keys=self.sort_keys, indent=self.indent).encode(_default)
        return output

def isiterable(obj):
    import collections
    if isinstance(obj, collections.Mapping):
        return True
    elif isinstance(obj, collections.Iterable):
        return True
    return False


def loads_s(string):
    return GqlEncoder().loads(string)


def dumps_s(obj, default=None, unpicklable=True, max_depth=None,
        sort_keys=DEBUG, indent=DEBUG):
    ret = GqlEncoder(sort_keys=sort_keys, indent=indent).encode(obj)
    if ret == 'null' and default is not None:
        return GqlEncoder(sort_keys=sort_keys, indent=indent).encode(default)
    return ret


def dump_s(obj, stream, default=None, unpicklable=True, max_depth=None,
        sort_keys=DEBUG, indent=DEBUG):
    ret = GqlEncoder(sort_keys=sort_keys, indent=indent).encode(obj)
    if ret == 'null' and default is not None:
        stream.write(GqlEncoder(sort_keys=sort_keys, indent=indent).encode(default))
    stream.write(ret)
    return stream

loads = simplejson.loads
load = simplejson.load
load_s = simplejson.load
dumps = simplejson.dumps
dump = simplejson.dump
encode = dumps
decode = loads
encode_s = dumps_s
decode_s = loads_s
