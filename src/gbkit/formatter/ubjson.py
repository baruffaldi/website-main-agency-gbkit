#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import os
from gbkit.contrib import simpleubjson

DEBUG = os.environ.get('SERVER_SOFTWARE').startswith("Dev")
if 'debug=1' in os.environ.get('QUERY_STRING', ''):
    DEBUG = True
if 'debug=0' in os.environ.get('QUERY_STRING', ''):
    DEBUG = False


def isiterable(obj):
    import collections
    if isinstance(obj, collections.Mapping):
        return True
    elif isinstance(obj, collections.Iterable):
        return True
    return False


def load(stream):
    return simpleubjson.decode(stream.read())

load_s = load
loads = simpleubjson.decode
loads_s = simpleubjson.decode


def dump(obj, stream, default=None, allow_noop=False, spec='draft8'):
    stream.write(simpleubjson.encode(obj, default, allow_noop, spec))
    return stream

dumps = simpleubjson.encode


def dumps_s(obj, default=None, allow_noop=False, spec='draft8'):
    from gbkit.formatter import pythonizer
    return simpleubjson.encode(
        pythonizer.encode(obj), default, allow_noop, spec)


def dump_s(obj, stream, default=None, allow_noop=False, spec='draft8'):
    stream.write(dumps_s(obj, default, allow_noop, spec))
    return stream

encode = dumps
decode = loads
encode_s = dumps_s
decode_s = loads_s
