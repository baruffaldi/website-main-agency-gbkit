#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1


class Py2XML():
    def __init__(self):
        self.data = ""  # where we store the processed XML string

    def isiterable(self, obj):
        return False

    def dumps(self, pythonObj, objName=None):
        '''
        processes Python data structure into XML string
        needs objName if pythonObj is a List
        '''
        if pythonObj == None:
            return ""

        import collections
        if isinstance(pythonObj, collections.Mapping):
            self.data = self._PyDict2XML(pythonObj)
        elif isinstance(pythonObj, collections.Iterable):
            self.data = self._PyList2XML(pythonObj, objName)
        else:
            self.data = "<%(n)s>%(o)s</%(n)s>" % {'n': objName, 'o': str(pythonObj)}
        return self.data

    def loads(self, pythonObj, objName=None):
        '''
        processes Python data structure into XML string
        needs objName if pythonObj is a List
        '''
        if pythonObj == None:
            return ""

        import collections
        if isinstance(pythonObj, collections.Mapping):
            self.data = self._PyDict2XML(pythonObj)
        elif isinstance(pythonObj, collections.Iterable):
            self.data = self._PyList2XML(pythonObj, objName)
        else:
            self.data = "<%(n)s>%(o)s</%(n)s>" % {'n': objName, 'o': str(pythonObj)}
        return self.data

    def _PyDict2XML(self, pyDictObj, objName=None):
        '''
        process Python Dict objects
        They can store XML attributes and/or children
        '''
        tagStr = ""      # XML string for this level
        attributes = {}  # attribute key/value pairs
        attrStr = ""     # attribute string of this level
        childStr = ""    # XML string of this level's children

        for k, v in pyDictObj.items():
            if isinstance(v, dict):
                # child tags, with attributes
                childStr += self._PyDict2XML(v, k)
            elif isinstance(v, list):
                # child tags, list of children
                childStr += self._PyList2XML(v, k)
            else:
                # tag could have many attributes, let's save until later
                attributes.update({k: v})

        if objName == None:
            return childStr

        # create XML string for attributes
        for k, v in attributes.items():
            attrStr += " %s=\"%s\"" % (k, v)

        # let's assemble our tag string
        if childStr == "":
            tagStr += "<%(n)s%(a)s />" % {'n': objName, 'a': attrStr}
        else:
            tagStr += "<%(n)s%(a)s>%(c)s</%(n)s>" % {'n': objName, 'a': attrStr, 'c': childStr}

        return tagStr

    def _PyList2XML(self, pyListObj, objName=None):
        '''
        process Python List objects
        They have no attributes, just children
        Lists only hold Dicts or Strings
        '''
        tagStr = ""    # XML string for this level
        childStr = ""  # XML string of children

        for childObj in pyListObj:

            if isinstance(childObj, dict):
                # here's some Magic
                # we're assuming that List parent has a plural name of child:
                # eg, persons > person, so cut off last char
                # name-wise, only really works for one level, however
                # in practice, this is probably ok
                childStr += self._PyDict2XML(childObj, objName[:-1])
            else:
                for string in childObj:
                    childStr += string

        if objName == None:
            return childStr

        tagStr += "<%(n)s>%(c)s</%(n)s>" % {'n': objName, 'c': childStr}

        return tagStr
