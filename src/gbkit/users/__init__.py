#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import os
import google.appengine.api.users
import gbkit.store.models.user_settings

create_login_url = google.appengine.api.users.create_login_url
CreateLoginURL = create_login_url
create_logout_url = google.appengine.api.users.create_logout_url
CreateLogoutURL = create_logout_url
get_current_user = google.appengine.api.users.get_current_user
GetCurrentUser = get_current_user
is_current_user_admin = google.appengine.api.users.is_current_user_admin
IsCurrentUserAdmin = is_current_user_admin

{
'HTTP_X_ZOO': 'app-id=shell-filippo-baruffaldi,domain=baruffaldi.info',
  'HTTP_X_APPENGINE_COUNTRY': 'IT',
   'HTTP_X_APPENGINE_CITYLATLONG': '45.463688,9.188140',
    'REMOTE_ADDR': '37.182.74.182',
      'USER_IS_ADMIN': '0',
        'USER_ORGANIZATION': '',
          'USER_EMAIL': '',
          'USER_ID': '',
          'HTTP_X_APPENGINE_CITY': 'milan',
     'SERVER_PORT': '80', 'HTTP_X_APPENGINE_REGION': 'mi',
          'HTTP_X_GOOGLE_APPS_METADATA': 'domain=baruffaldi.info',
          'HTTP_ACCEPT_LANGUAGE': 'it-IT,it;q=0.8,en-US;q=0.6,en;q=0.4',
   'HTTP_ACCEPT_CHARSET': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
   'HTTP_USER_AGENT': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.83 Safari/537.1',
   'AUTH_DOMAIN': 'gmail.com',
    'USER_NICKNAME': '',
    'USER': 'apphosting'}


class User(google.appengine.api.users.User):
    settings_data = None
    settings = {}
    useragent = os.environ["HTTP_USER_AGENT"]
    device = {}

    def __init__(self, email='', _auth_domain=None, _user_id=None,
        federated_identity=None, federated_provider=None,
        _strict_mode=True):
        self.settings_data = gbkit.store.models.user_settings\
            .UserSettings.all().filter('user =', email).fetch(1000, 0)

        for setting in self.settings_data:
            self.settings[setting.name] = setting.value

        # Google defined part
        if _auth_domain is None:
            _auth_domain = os.environ.get('AUTH_DOMAIN')
        assert _auth_domain

        if email is None and federated_identity is None:
            email = os.environ.get('USER_EMAIL', email)
            _user_id = os.environ.get('USER_ID', _user_id)
            federated_identity = os.environ.get('FEDERATED_IDENTITY',
                                              federated_identity)
            federated_provider = os.environ.get('FEDERATED_PROVIDER',
                                              federated_provider)

        if email is None:
            email = ''

        if not email and not federated_identity and _strict_mode:
            raise google.appengine.api.users.UserNotFoundError

        self.__email = email
        self.__federated_identity = federated_identity
        self.__federated_provider = federated_provider
        self.__auth_domain = _auth_domain
        self.__user_id = _user_id or None

    @staticmethod
    def create_login_url(*args, **kwargs):
        return '/gbkit/users/login'

    @staticmethod
    def create_logout_url(*args, **kwargs):
        return '/gbkit/users/logout'

    @staticmethod
    def get_current_user():
        return get_current_user()

    @staticmethod
    def is_current_user_admin():
        return is_current_user_admin()

    def is_current_user_desktop(callback=None, *args, **kwargs):
        if 'Windows' in os.environ.get("HTTP_USER_AGENT"):
            if callback:
                callback(*args)
            return True
        return False
