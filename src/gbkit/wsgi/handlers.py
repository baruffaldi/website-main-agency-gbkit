#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import re
import os
import cgi
import datetime
import time
import logging
import operator
import random
import base64
import uuid
import pprint
import cStringIO as StringIO

import webapp2
import webapp2_extras.sessions
import webapp2_extras.appengine.sessions_memcache
import webapp2_extras.appengine.sessions_ndb

import google.appengine.api.users
from google.appengine.ext import ereporter

import appengine_config
import django_config
import gbkit_config

from gbkit.errors import Error
from gbkit import environment
from gbkit import formatter
from gbkit import store
from gbkit import logger
from gbkit import mimetypes
from gbkit import template
from gbkit import users

template.register_template_library('gbkit.template.helpers')
ereporter.register_logger()

DEBUG = gbkit_config.ENVIRONMENT_DEBUG
config = gbkit_config

wsgi_config = {
    #'webapp2_extras.jinja2': {
    #    'template_path': 'templates',
    #    'filters': {
    #        'timesince': filters.timesince,
    #        'datetimeformat': filters.datetimeformat
    #    },
    #    'environment_args': {'extensions': ['jinja2.ext.i18n']},
    #},
    'webapp2_extras.sessions': {
        'secret_key': gbkit_config.SECRET_KEY,
        'backends': {
            'datastore': 'webapp2_extras.appengine.sessions_ndb.DatastoreSessionFactory',
            'memcache': 'webapp2_extras.appengine.sessions_memcache.MemcacheSessionFactory',
            'securecookie': 'webapp2_extras.sessions.SecureCookieSessionFactory'
        }
    }
}


def dump(var):
    cb = StringIO.StringIO()
    pprint.pprint(var, cb)
    return cb.read()


def empty_function(self, *args, **kwargs):
    pass


class CsrfException(Exception):
    pass


class GBKitException(Exception):
    """ GBKit Generic Exception """
    code = 0
    text = "Unknown Error"


class GBKitNotFoundException(GBKitException):
    code = 0
    text = "The requested content is unavailable"

    def __init__(self, code=0, text=None):
        if code:
            self.code = code
        if text:
            self.text = text


def LoggerDisabledDecorator(original_class):
    original_class.log = empty_function
    original_class.d_log = empty_function
    original_class.i_log = empty_function
    return original_class


class LoggerDisabledMetaClass(type):
    def __new__(cls, name, bases, attrs):
        attrs['log'] = empty_function
        attrs['d_log'] = empty_function
        attrs['i_log'] = empty_function
        return super(LoggerDisabledMetaClass, cls).__new__(
            cls, name, bases, attrs)


class RequestHandler(webapp2.RequestHandler):
    """ Options to customize on your RequestHandler """
    cache_enabled = False                       # Cache Enabled
    csrf_protection_enabled = False             # CSRF Protection Enabled
    csrf_protection_check_enabled = True        # CSRF Check Enabled
    csrf_token = None                           # CSRF Protection Token
    api_mode = False                            # Force HTTP Status Codes
    debug_on_headers = False                    # Debug on Cookies

    """You can protect this options setting it on your request handler"""
    db_stats = None                            # AppEngine datastore stats
    p3p_enabled = None                         # P3P HTTP header enabled
    render_callback = None                     # Error TPL render callback
    check_capabilities = None                  # Force capabilities check
    forced_attachment = None                   # Force attach. disposition
    forced_user_output = None                  # Force user info output
    forced_factories_output = None             # Force factories output
    forced_environment_settings_output = None  # Force environment output
    forced_handler_settings_output = None      # Force handlerconf output
    forced_secure_encoding = None              # Force secure output enc
    forced_get_requests_lock = None            # Force secure output enc
    forced_post_requests_lock = None           # Force secure output enc
    api_httpcodes_enabled = True               # Send HTTP response hdrs
    frame_options_header = None                # Send HTTP response hdrs

    """ GBKit Request Handler Variables """
    debug = gbkit_config.ENVIRONMENT_DEBUG    # DEBUG

    environment = environment                 # GBKit Environment facility
    logging = logger                          # GBKit Logging facility
    mimetypes = mimetypes                     # GBKit Mimetypes facility
    template = template                       # GBKit Tpl Engine facility
    cookies = None                            # GBKit Cookies facility
    memcache = None                           # GBKit Memcache facility
    users = users                             # GBKit Users facility

    s = store                                 # GBKit DataStore facility
    store = store                             # GBKit DataStore facility
    f = formatter                             # GBKit Formatter facility
    formatter = formatter                     # GBKit Formatter facility

    user = {}                                 # Current User Handler
    device = {}                               # Current User Agent Info

    _appengine_settings = appengine_config    # AppEngine Settings
    _wsgi_settings = wsgi_config              # wsgi Settings
    _django_settings = django_config          # Django Settings
    _gbkit_settings = gbkit_config            # GBKit Settings

    uri = None                                # Request URI
    mimetype = 'text/html'                    # Request Mimetype
    _requested_format = 'html'                # Request Format
    _tpl_vars = {}                            # Template Rendering Vars
    _secret_key = 'gbkit-secret-static-key-nobody-can-hack-my-mind'
                                              # GBKit Static Secret Key
    _request_secret_key = None
                                             # GBKit Dynamic Secret Key
    _services_disabled = []                  # AppEngine Disabled Services
    _services = gbkit_config.APPENGINE_SERVICES  # AppEngine Services
    _services_capabilities = gbkit_config.APPENGINE_SERVICES_CAPABILITIES
                                             # AppEngine Sub-Services

    # ------------------------------------------------------------------ #
    #                          INITIALIZATION                            #
    # ------------------------------------------------------------------ #

    """
    Initialize Request handler and Session factory
    """
    def __init__(self, *args, **kwargs):
        self._session = webapp2_extras.sessions.get_store(
            request=self.request
        )
        super(RequestHandler, self).__init__(*args)
        self.itemlist = dir(self)

    """
    Session Handling and Dispatch the Request
    """
    _session = None

    def dispatch(self):
        self.log("[GBKit] - Dispatch")
        self.log("[GBKit] %s" % ("-" * 37))
        try:
            # Dispatch the request.
            super(RequestHandler, self).dispatch()
        except:
            pass
        finally:
            # Save all sessions.
            if self._session:
                self._session.save_sessions(self.response)
            self.log("[GBKit] %s" % ("-" * 37))
            self.log("[GBKit] - Headers sent")
            self.log("[GBKit] %s" % ("-" * 37))
            headers_lenght = 0
            for row in str(self.response.out).split('\n'):
                if not row.split():
                    break
                headers_lenght += 1
                self.log("[GBKit] Header %s" % row)
            self.log("[GBKit] %s" % ("-" * 37))
            self.log("[GBKit] Payload %s lines sent" \
                % str(len(str(self.response.out).split("\n")) \
                - headers_lenght - 1))
            self.log("[GBKit] %s" % ("-" * 37))
            self.log('[GBKit] - %s Request processing done.' \
                % self.request.method)
            self.log("[GBKit] %s" % ("-" * 37))
            if self.db_stats:
                from google.appengine.ext.db import stats
                global_stat = stats.GlobalStat.all()
                if global_stat:
                    self.log("[GBKit] - AppEngine Datastore Statistics")
                    self.log("[GBKit] %s" % ("-" * 37))
                    self.i_log('[GBKit] Total bytes stored: %d' % global_stat.bytes)
                    self.i_log('[GBKit] Total entities stored: %d' % global_stat.count)

    @webapp2.cached_property
    def session(self):
        # Returns a session using the default cookie key.
        if not self._session:
            self._session = webapp2_extras.sessions.get_store(request=self.request)
        return self._session.get_session(name="gbkit-sid")

    """
    Initialize the kit
    """
    def initialize(self, request, response):
        self._request_secret_key = self.new_dynamic_key()

        self.init_log()
        self.log("[GBKit] logging factory started.")

        self.init_debug(request)
        self.log("[GBKit] debug factory started.")

        # webapp2 initialize
        super(RequestHandler, self).initialize(request, response)
        self.log("[GBKit] - webapp2 framework initialized")

        self.log("[GBKit] session factory started.")

        ### INIT STORE
        self.init_cookies()
        self.log("[GBKit] cookies factory started.")

        self.init_datastore()
        self.log("[GBKit] datastore factory started.")

        self.init_environment()
        self.init_environment_check()
        self.log("[GBKit] environment factory started.")

        self.init_cache()
        self.log("[GBKit] cache factory started.")

        self.init_user()
        self.log("[GBKit] user factory started.")

        self.init_device()
        self.log("[GBKit] user device recognized.")

        self.init_csrf_protection()
        self.log("[GBKit] - Ready.")
        self.log("[GBKit] %s" % ("-" * 37))
    """
    Debug Handling
    """
    def init_debug(self, request=None):
        # Debug mode detection
        self.debug = gbkit_config.ENVIRONMENT_DEBUG
        if request:
            if request.get('debug', default_value=False) \
            or request.get('debugrecursive', default_value=False):
                self.debug = bool(request.get('debug', default_value=0, type=int)) \
                    or bool(request.get('debugrecursive', default_value=0, type=int))
        if os.environ.get('HTTP_REFERER', False):
            ref_get_vs = os.environ.get('HTTP_REFERER', '').split('?')[-1]
            if 'debugrecursive=' in ref_get_vs:
                self.debug = bool(int(ref_get_vs.split('=')[1]))
        if request:
            if request.get('debug_on_headers', default_value=False):
                self.debug_on_headers = request.get('debug_on_headers')

    """
    Logs Handling
    """
    def init_log(self):
        # Log facility
        self.logging = self.logging.Factory(debug=self.debug)

        self.log("[GBKit] %s" % ("-" * 37))
        self.log("[GBKit] - Request received from %s." % (
            os.environ.get('REMOTE_ADDR', 'unknown')
        ))

    """
    Cookies Handling
    """
    def init_cookies(self, *args, **kwargs):
        #self.cookies = self.cookies.Factory(
        #    request_handler=self, secret_key=self._secret_key)
        #
        self.cookies = store.cookies.Factory(self, self._secret_key)

    """
    Datastore Handling
    """
    def init_datastore(self, *args, **kwargs):
        self.db = store.db
        pass

    """
    Initialization processes
    """
    def init_environment(self):
        self.environment = self.environment.Factory()

        if self.request.get('check_capabilities', default_value=False) \
        and self.check_capabilities is None:
            self.check_capabilities = bool(int(self.request.get(
                'check_capabilities', default_value=False)))
            self.log("[GBKit] forced check capabilities detected." \
                % 'enabled' if self.check_capabilities else 'disabled')

        if self.request.get('format', default_value=False):
            format = self.request.get('format').strip().lower()
            if format in gbkit_config.ENABLED_FORMAT:
                self._requested_format = format
                self.log("[GBKit] forced format detected.")

        if self.request.get('db_stats', default_value=False) \
        and self.db_stats is None:
            self.db_stats = bool(int(self.request.get(
                'db_stats', default_value=False)))
            self.log("[GBKit] forced database statistics %s detected." \
                % 'enabled' if self.db_stats else 'disabled')

        if self.request.get('p3p_enabled', default_value=False) \
        and self.p3p_enabled is None:
            self.p3p_enabled = bool(int(self.request.get(
                'p3p_enabled', default_value=False)))
            self.log("[GBKit] forced p3p %s detected." \
                % 'enabled' if self.p3p_enabled else 'disabled')

        if self.request.get('forced_attachment', default_value=False) \
        and self.forced_attachment is None:
            self.forced_attachment = bool(int(self.request.get(
                'forced_attachment', default_value=False)))
            self.log("[GBKit] forced attachment disposition detected.")

        if self.request.get('forced_user_output', default_value=False) \
        and self.forced_user_output is None:
            self.forced_user_output = bool(int(self.request.get(
                'forced_user_output', default_value=False)))
            self.log("[GBKit] forced 'user' output detected.")

        if self.request.get('forced_factories_output',
            default_value=False) \
        and self.forced_factories_output is None:
            self.forced_factories_output = bool(int(self.request.get(
                'forced_factories_output', default_value=False)))
            self.log("[GBKit] forced 'factories' output detected.")

        if self.request.get('forced_environment_settings_output',
            default_value=False) \
        and self.forced_environment_settings_output is None:
            self.forced_environment_settings_output = bool(int(self.request.get(
                'forced_environment_settings_output', default_value=False)))
            self.log("[GBKit] forced 'environment' output detected.")

        if self.request.get('forced_handler_settings_output',
            default_value=False) \
        and self.forced_handler_settings_output is None:
            self.forced_handler_settings_output = bool(int(self.request.get(
                'forced_handler_settings_output', default_value=False)))
            self.log("[GBKit] forced 'handler settings' output detected.")

        self.response.headers.add(
            'X-Generator', gbkit_config.PACKAGE_NAME)

    def init_environment_check(self):
        if self.check_capabilities:
            from google.appengine.api import capabilities
            for service in gbkit_config.APPENGINE_SERVICES:
                if not capabilities.CapabilitySet(service).is_enabled():
                    self._services_disabled.append(service)

                if service in gbkit_config.APPENGINE_SERVICES_CAPABILITIES:
                    capability = gbkit_config.APPENGINE_SERVICES_CAPABILITIES[service]
                    service_cap = "%s-%s" % (service, "-".join(capability))
                    if not capabilities.CapabilitySet(service, capability).is_enabled() \
                    and service_cap not in self._services_disabled:
                        self._services_disabled.append(service_cap)

            for capability in self._services_disabled:
                self.wlog("[GBKit] AppEngine %s capability disabled."
                    % capability)

    """
    CSRF Protection
    """
    def init_csrf_protection(self):
        # CSRF Protection
        if self.csrf_protection_enabled:
            self.init_csrf()
            self.i_log("[GBKit] CSRF protection activated.")
            self.response.headers['X-XSS-Protection'] = "1"

            if self.csrf_protection_check_enabled:
                self.i_log("[GBKit] CSRF protection check activated.")
                self.response.headers['X-XSS-Protection-Check'] = "1"
                try:
                    if self.request.method == 'POST':
                        self.check_xsrf()
                except:
                    self.w_log("[GBKit] CSRF token not valid or missing.")
            else:
                self.i_log("[GBKit] CSRF protection check disabled.")
                self.response.headers['X-XSS-Protection-Check'] = "0"

        else:
            self.response.headers['X-XSS-Protection'] = "0"

    def get_csrf(self):
        csrf_token = None#self.request.get("_csrf_token", default_value=None)
        if csrf_token is None:
            csrf_token = self.cookies.get_cookie('_csrf_token')
        if csrf_token is None:
            csrf_token = self.request.headers.get("X-XSRFToken")
        if csrf_token is None:
            csrf_token = self.request.headers.get("X-CSRFToken")
        return csrf_token

    def init_csrf(self):
        """Issue and handle CSRF token as necessary"""
        self.csrf_token = self.get_csrf()
        if self.csrf_token is None:
            self.csrf_token = str(uuid.uuid4())[:8]
            self.cookies.set_cookie('_csrf_token', self.csrf_token)
            self.response.headers.add('X-XSRFToken', self.csrf_token)
            self.response.headers.add('X-CSRFToken', self.csrf_token)
            self.i_log("[GBKit] CSRF token set.")

    def check_xsrf(self):
        if self.csrf_protection_enabled:
            csrf_token = self.get_csrf()
            if csrf_token is None:
                raise CsrfException(
                    'Missing CSRF token.')

            if self.request.method == 'POST' \
               and self.csrf_protection_enabled \
               and self.csrf_token != csrf_token:
                raise CsrfException('Invalid CSRF token.')
                return False
        return True

    """
    Cache Handling
    """
    def init_cache(self):
        self.memcache = store.memcache.Factory()

        if self.debug:
            self.cache_enabled = False
        if not self.cache_enabled:
            if self.request.get('cache', default_value=False):
                self.cache_enabled = self.request.get('cache') == "1"
        if self.cache_enabled:
            self.log("[GBKit] cache enabled.")
        else:
            self.log("[GBKit] - Cache disabled.")

    """
    User Handling
    """
    def init_user(self, *args, **kwargs):
        # TODO user detect
        self.user = self.users.User(
            email=os.environ.get('USER_EMAIL'), _strict_mode=False)
        try:
            self._user = google.appengine.api.users.get_current_user()
        except:
            pass
        self.log("[GBKit] user detected.")
        pass

    """
    UserAgent Detection
    """
    def init_device(self, *args, **kwargs):
        # TODO device detect
        self.log("[GBKit] device detected.")
        pass

    # ------------------------------------------------------------------ #
    #                        INITIALIZATION ENDS                         #
    #                              - * -                                 #
    #                            FALLBACKS                               #
    # ------------------------------------------------------------------ #

    """
    GET and POST Fallbacks
    """
    def get(self, *args, **kwargs):
        """ If no GET method it falls back to POST method """
        if not self.forced_get_requests_lock:
            self.post(*args, **kwargs)

    def post(self, *args, **kwargs):
        """ If no POST method it falls back to GET method """
        if not self.forced_post_requests_lock:
            self.get(*args, **kwargs)

    def head(self, *args, **kwargs):
        """ If no HEAD method it falls back to GET method """
        self.get(*args, **kwargs)

    def get_vars(self):
        """ Returns all GET variables keys and values """
        returndict = {}
        for (x, y) in self.request.GET.items():
            returndict[x] = y
        return returndict

    def post_vars(self):
        """ Returns all POST variables keys and values """
        returndict = {}
        for (x, y) in self.request.POST.items():
            returndict[x] = y
        return returndict

    def request_vars(self):
        """ Returns all GET (and POST if presents) variables keys and
            values """
        returndict = {}
        returnlist = self.request.GET.items()
        if os.environ.get('REQUEST_METHOD', False).upper() == 'POST':
            returnlist.extend(self.request.POST.items())
        for key, value in returnlist:
            returndict[key] = value
        return returndict

    # ------------------------------------------------------------------ #
    #                          FALLBACKS ENDS                            #
    #                              - * -                                 #
    #                             HELPERS                                #
    # ------------------------------------------------------------------ #

    """
    Cookie Message Methods:
        - set_message()
        - get_message()
    """
    def set_message(self, **obj):
        """Simple message support"""
        self.cookies.set_cookie('m', base64.b64encode(
            self.serialize(obj, 'json')) if obj else None)

    def get_message(self):
        """Get and clear the current message"""
        message = self.cookies.get_cookie('m')
        if message:
            self.set_message()
            return self.unserialize(base64.b64decode(message), 'json')

    """
    Request Registry Methods:
        - get_registry()
        - get_registry_item()
        - set_registry_item()
    """
    @staticmethod
    def registry():
        app = webapp2.get_app()
        return app.registry

    def get_registry_item(self, key, default=None):
        return self.registry().get(key, default)

    def set_registry_item(self, key, value=None):
        try:
            self.registry()[key] = value
        except:
            return False
        finally:
            return True

    # ------------------------------------------------------------------ #
    #                           HELPERS ENDS                             #
    #                              - * -                                 #
    #                        TEMPLATE HANDLING                           #
    # ------------------------------------------------------------------ #

    """
    Template and Path Handling
        - render(ex, tpl, data, debug, ret)
        - format()
        - get_template_vars(dict)
        - get_templates_path(*string)
        - get_gbkit_templates_path(*string)
        - get_static_url()
        - strip_path_from_filename(pathstring)
        - strip_filename_from_path(pathstring)
        - get_relative_path(pathstring)
        - get_splitted_path(pathstring)
    """
    def render(self, tpl, data=None, debug=None, ret=False, error=None, default=None):
        self._tpl_vars.update(self.get_template_vars(data, default))
        if error is not None:
            self._tpl_vars['error'] = error
        self._tpl_vars['debug'] = debug if debug is not None \
            else self.debug
        output = None

        if self.format() == 'html' or tpl is not None:
            if not os.path.isabs(tpl):
                tpl = os.path.abspath(tpl)

            t = os.environ.get('PATH_TRANSLATED', '').replace(
                str(self.environment.basepath), '')

            # TODO find a hack to update TEMPLATE_DIRS, this alg sux
            c = 0
            new_tpl_dirs = ()
            for path in django_config.TEMPLATE_DIRS:
                if c == 3:
                    new_tpl_dirs += (RequestHandler.get_gbkit_templates_path(
                        ".".join(t.split(os.path.sep)[-1].split('.')[:-1])),)
                    c += 1
                    pass
                if c == 0:
                    new_tpl_dirs += (RequestHandler.get_templates_path(
                        "_".join(t.split(os.path.sep)[-1].split('.')[:-1])),)
                    c += 1
                    pass

                new_tpl_dirs += (path,)
                c += 1

            if not os.path.exists(tpl):
                self.raise_error("Template '%s' not found" % tpl)
        else:
            self.log("[GBKit] No template needed by request")

        self.set_headers()
        self.log("[GBKit] Output Format: %s (%s)" % \
            (self.format().upper(), self.mimetype))

        if self.format() == 'html':
            output = self.template.render(
                template_path=tpl, template_dict=self._tpl_vars,
                debug=self._tpl_vars['debug'], template_dirs=new_tpl_dirs)
            self.i_log("[GBKit] Template rendering: " + tpl.replace(
                django_config.PROJECT_ROOT, '').replace('\\', '/'))
        elif self.format() in gbkit_config.ENABLED_FORMAT:
            try:
                if self.forced_secure_encoding:
                    exec('output = formatter.%s.dumps_s(self._tpl_vars)'\
                     % self.format())
                else:
                    exec('output = formatter.%s.dumps(self._tpl_vars)' \
                         % self.format())
            except Exception, e:
                try:
                    self.i_log('[GBKit] WARNING: %s' % (e.message))
                    exec('output = formatter.%s.dumps_s(self._tpl_vars)'\
                     % self.format())
                    e = None
                except Exception, ee:
                    self.i_log('[GBKit] WARNING: requested format (%s) is not working. -- %s' % (self.format(),ee.message))
        else:
            self.i_log('[GBKit] WARNING: requested format (%s) is not enabled.' % self.format())
            raise Exception('Requested format (%s) not found' \
                % self.format())

        if output is None:
            self.raise_error('[GBKit] WARNING: Requested format (%s) has failed -- %s' \
                % (self.format(),str(e) or str(ee) or ''))
        if ret:
            return output

        self.response.out.write(output)

    def get_template_vars(self, data=None, default=None):
        data = {u'data': data if data is not None else default}

        if isinstance(data[u'data'], list):
            data[u'length'] = len(data[u'data'])

        if self.format() == 'html' \
        or self.forced_handler_settings_output \
        or self.forced_environment_settings_output \
        or self.forced_factories_output \
        or self.forced_user_output \
        or self.api_mode:
            data[u'gbkit'] = {}
            data[u'gbkit'][u'debug'] = self.debug
            data[u'gbkit'][u'request_vars'] = self.request.arguments()

        if self.format() == 'html' \
        or self.forced_handler_settings_output \
        or self.api_mode:
            data[u'gbkit'][u'cache_enabled'] = self.cache_enabled
            data[u'gbkit'][u'csrf_token'] = self.csrf_token
            data[u'gbkit'][u'csrf_protection_enabled'] = self.csrf_protection_enabled

        if self.format() == 'html' \
        or self.forced_environment_settings_output:
            data[u'gbkit'][u'appengine_settings'] = self._appengine_settings
            data[u'gbkit'][u'django_settings'] = self._django_settings
            data[u'gbkit'][u'gbkit_settings'] = self._gbkit_settings

            data[u'gbkit'][u'services'] = self._services
            data[u'gbkit'][u'services_capabilities'] = self._services_capabilities
            data[u'gbkit'][u'services_disabled'] = self._services_disabled

        if self.format() == 'html' \
        or self.forced_factories_output:
            data[u'gbkit'][u'logging'] = self.logging
            data[u'gbkit'][u'cookies'] = self.cookies
            data[u'gbkit'][u'mimetypes'] = self.mimetypes
            data[u'gbkit'][u'formatter'] = self.formatter
            data[u'gbkit'][u'f'] = self.formatter
            data[u'gbkit'][u'users'] = self.users
            data[u'gbkit'][u'environment'] = self.environment

        if self.format() == 'html' \
        or self.forced_user_output \
        or self.api_mode:
            data[u'gbkit'][u'user'] = self.user
            data[u'gbkit'][u'_user'] = google.appengine.api.users.get_current_user()
            data[u'gbkit'][u'device'] = self.user.device
            data[u'gbkit']['login_url'] = '/gbkit/users/login?url=%s' % self.request.uri
            data[u'gbkit']['logout_url'] = '/gbkit/users/logout?url=%s' % self.request.uri

        if self.format() == 'html':
            data[u'gbkit'][u'secret_key'] = self._secret_key
            data[u'gbkit'][u'request_secret_key'] = self._request_secret_key

        #data[u'gbkit'][u'settings'] = self.settings()

        #data[u'gbkit']['facebook_app_id'] = facebookconf.FACEBOOK_APP_ID
        #data[u'gbkit']['canvas_name'] = facebookconf.FACEBOOK_CANVAS_NAME
        #data[u'gbkit']['current_user'] = self.current_user
        #data[u'gbkit']['logged_in_user'] = self._user
        #
        """\
            (google.appengine.api.users.create_logout_url(self.request.uri) if google.appengine.api.users.get_current_user() else 'https://www.facebook.com/dialog/oauth?client_id='
              + facebookconf.FACEBOOK_APP_ID + '&redirect_uri='
             + self.request.uri)"""
        return data

    """
    - HTTP
        - set_headers()
    """
    def set_headers(self):
        format = self.format()

        #if self.debug and self.user.is_current_user_admin():
        #    self.response.headers.add(
        #        'X-GBKit-Vars', formatter.json.dumps(self._tpl_vars))

        # Cache-Control
        self.response.headers['Cache-Control'] = "private, no-cache, no-store, must-revalidate"
        self.response.headers['Pragma'] = "no-cache"
        self.response.headers['Expires'] = "Sat, 01 Jan 2000 00:00:00 GMT"
        self.response.headers['Strict-Transport-Security'] = "max-age=0"

        self.response.headers['X-Content-Type-Options'] = "nosniff"
        if self.frame_options_header is not None:
            self.response.headers['X-Frame-Options'] = self.frame_options_header
        """self.response.headers['abc'] = "abc"
Connection:keep-alive
Content-Encoding:gzip
Transfer-Encoding:chunked



X-WebKit-CSP:default-src *;script-src https://*.facebook.com http://*.facebook.com https://*.fbcdn.net http://*.fbcdn.net *.facebook.net *.google-analytics.com *.virtualearth.net *.google.com 127.0.0.1:* *.spotilocal.com:* chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl 'unsafe-inline' 'unsafe-eval' https://*.akamaihd.net http://*.akamaihd.net;style-src * 'unsafe-inline';connect-src https://*.facebook.com http://*.facebook.com https://*.fbcdn.net http://*.fbcdn.net *.facebook.net *.spotilocal.com:* https://*.akamaihd.net ws://*.facebook.com:* http://*.akamaihd.net;

        self.response.headers['abc'] = "abc"""

        # Content-Disposition Header
        self.response.headers.add('Content-Disposition', \
            str('%s%s' %
                ('attachment' if self.forced_attachment else 'inline',
                ('; filename=contents.%s' % format) if format != 'html' \
                    else '')
            ))

        # Content-Type Header
        if '.%s' % format in self.mimetypes.mimetypes.types_map[0]:
            self.mimetype = self.mimetypes.mimetypes.types_map[0]['.%s' % format]
            self.response.headers['Content-Type'] = str('%s; charset=utf-8' % self.mimetype)
        elif '.%s' % format in self.mimetypes.mimetypes.types_map[1]:
            self.mimetype = self.mimetypes.mimetypes.types_map[1]['.%s' % format]
            self.response.headers['Content-Type'] = str('%s; charset=utf-8' % self.mimetype)
        elif format == 'jsonp':
            self.response.headers['Content-Type'] = str('%s; charset=utf-8' % 'text/javascript')
        elif format == 'htmlp':
            self.response.headers['Content-Type'] = str('%s; charset=utf-8' % 'text/html')

        # P3P Policies
        if not self.p3p_enabled and 'P3P' not in self.response.headers:
            self.response.headers.add('P3P', 'CP="gbkit does not support P3P policies. P3P standard is out of date. Refer to CSP."')

        self.log("[GBKit] - Headers has been set")
        self.log("[GBKit] %s" % ("-" * 37))

    def format(self):
        return self._requested_format

    @staticmethod
    def get_template(*paths):
        """ Returns requested template path """
        filepath = os.path.join(*paths)
        path = RequestHandler.strip_filename_from_path(filepath)

        check_paths = list(django_config.TEMPLATE_DIRS)
        t = os.environ.get('PATH_TRANSLATED', '').replace(str(
            environment.Factory.basepath), '')

        check_paths.insert(3, RequestHandler.get_gbkit_templates_path(
            ".".join(t.split(os.path.sep)[-1].split('.')[:-1])))
        check_paths.insert(1, RequestHandler.get_templates_path(
            "_".join(t.split(os.path.sep)[-1].split('.')[:-1])))

        for path in check_paths:
            if os.path.exists(os.path.join(path, filepath)):
                return os.path.join(path, filepath)

        logging.warn("[GBKit] Trying to load a missing template")
        logging.warn("[GBKit] Path: %s" % filepath)
        if not gbkit_config.ENVIRONMENT_DEBUG:
            logging.warn("[GBKit] Fallback to courtesy page: %s" % \
                os.path.join('errors', 'default_error.html'))
            return RequestHandler.path('errors', 'default_error.html')

        return filepath

    @staticmethod
    def strip_path_from_filename(path):
        """ Returns gbkit template path """
        return path.split(path)[1]

    @staticmethod
    def strip_filename_from_path(path):
        """ Returns gbkit template path """
        return path.split(path)[0]

    @staticmethod
    def get_relative_path(path):
        """ Returns gbkit template path """
        return path.replace(gbkit_config.BASEPATH, '')

    @staticmethod
    def get_splitted_path(path):
        """ Returns gbkit template path """
        return path.split(path)

    @staticmethod
    def get_templates_path(*paths):
        """ Returns application template path """
        return os.path.join(
            RequestHandler.path('application', 'views'),
            os.path.join(*paths) if paths else ''
        )

    @staticmethod
    def get_gbkit_templates_path(*paths):
        """ Returns gbkit template path """
        return os.path.join(
            RequestHandler.path('gbkit', 'views'),
            os.path.join(*paths) if paths else ''
        )

    @staticmethod
    def get_static_url():
        """ Returns application static URL """
        return '/static'

    @staticmethod
    def get_gbkit_static_url():
        """ Returns gbkit static URL """
        return '/gbkit/static'

    # ------------------------------------------------------------------ #
    #                           HELPERS ENDS                             #
    #                              - * -                                 #
    #                         ERRORS HANDLING                            #
    # ------------------------------------------------------------------ #

    """
    Errors(Exceptions) Handling
        - raise_error(text, code, details, exc, id)
        - handle_exception(exc, debug_mode)
        - error()
    """
    def raise_error(self, text='', code=0, details='', exc=None, id=None):
        if exc is not None:
            raise exc
            return

        #error = Error(code, text+details, details, exc, log=self.debug)
        #self.handle_exception(error=error, http_error=False, id=id)
        self.error(code, text)


    def error(self, code, message=False, error=None, debug_mode=False, render_html=False, render_callback=None, http_error=True, id=None):
        if error is None:
            error = Error(code=code, text=message)
        else:
            if int(code) == 0:
                code = error.code

        if self.api_mode:
            code = int(code)
            if code not in self._gbkit_settings.HTTP_STATUSES.keys():
                code = 500
        elif int(code) not in self._gbkit_settings.HTTP_STATUSES.keys() \
            or self.environment.is_production:
            code = 200

        if not self.api_httpcodes_enabled:
            self.response.clear()
        if self.api_httpcodes_enabled:
            self.response.set_status(int(code), str(error.text))

        self.response.headers.add("X-GBKit-Error-Code", str(code))
        self.response.headers.add("X-GBKit-Error-Text", str(message))
        if self.debug_on_headers:
            #if self.environment.is_development \
            #or self.user.is_current_user_admin():
            #    self.response.headers.add("X-GBKit-Environment", formatter.json.dumps_s(self))
            if error.request_id is not None:
                self.response.headers.add("X-GBKit-Error-Request-Id",
                str(error.request_id))
            if error.request_id_hash is not None:
                self.response.headers.add("X-GBKit-Error-Request-Id-Hash",
                str(error.request_id_hash))
            if error.request_log_id is not None:
                self.response.headers.add("X-GBKit-Error-Log-Id",
                str(error.request_log_id))
            if error.filename is not None:
                self.response.headers.add("X-GBKit-Error-Filename",
                str(error.filename.replace(gbkit_config.BASEPATH, '')))
            if error.row is not None:
                self.response.headers.add("X-GBKit-Error-Row",
                str(error.row))
            if error.parent is not None:
                self.response.headers.add("X-GBKit-Error-Parent",
                str(error.parent))
            if error.rowcontent is not None:
                self.response.headers.add("X-GBKit-Error-Line",
                str(error.line))
            if error.text is not None:
                self.response.headers.add("X-GBKit-Error-Message",
                str(error.text))
            if error.classname is not None:
                self.response.headers.add("X-GBKit-Error-Exception",
                str(error.classname))
            if error.thread is not None:
                self.response.headers.add("X-GBKit-Error-Thread",
                str(error.thread))
            if int(error.code):
                self.response.headers.add("X-GBKit-Error-Code",
                str(error.code))
            if error.details is not None:
                self.response.headers.add("X-GBKit-Error-Details",
                str(error.details))
            if error.string_traceback is not None:
                self.response.headers.add("X-GBKit-Error-Last-Working",
                str(error.string_traceback))

            if self.debug or self.environment.is_development \
            or self.user.is_current_user_admin():
                if error.string_stack is not None:
                    self.response.headers.add("X-GBKit-Error-Stack",
                    str(error.string_stack))

        debug = self.debug

        if self.format() == 'html':
            render_html = True
        if not render_callback:
            render_callback = self.render_callback

        if render_html and render_callback:
            self.native_render(render_callback(
                error, debug_mode=debug_mode,
                render_html=render_html, id=id))
        elif render_html:
            tpl = "default_error.html"
            if error.code and not error.code in [0, "0"]:
                tpl = "%s.html" % str(error.code)
            if id is not None:
                tpl = "%s.html" % id
            if error.classname == 'DeadlineExceededError':
                tpl = 'timeout.html'
            #if exc:
            #    if exc.__class__.__name__ == 'DeadlineExceededError':
            #        tpl = 'timeout.html'
            # TODO Inserire gli hook per le diverse eccezioni Deadline etc
            self.render(self.get_template('errors', tpl), error=error)
        else:
            self.render(None, error=error)
        if self.user.is_current_user_admin() and \
        ((self.environment.is_development and not debug) \
        or (self.environment.is_production and debug)):
            self.response.out.write(error.get_html())

    def handle_exception(self, exc=None, debug_mode=None, error=None):
        if exc is not None and error is None:
            error = Error(exception=exc, log=self.debug)

        self.error(error.code, error.text, error)

    # ------------------------------------------------------------------ #
    #                      TEMPLATE HANDLING ENDS                        #
    #                              - * -                                 #
    #                            UTILITIES                               #
    # ------------------------------------------------------------------ #

    """
    Useful Methods

    - Class
        - __setitem__(string, string)
        - __iter__()
        - keys()
        - values()
        - itervalues()
    - Log
        - log(text)
        - l_log(*args)
        - d_log(text)
        - i_log(text)
        - w_log(text)
        - e_log(text)
        - f_log(text)
        - c_log(text)
        - exc_log(text)
    - Path
        - path(*string)
    - Time
        - timestamp_now()
        - timestamp(datetime.datetime)
    - String
        - unicode_to_string(data)
        - string_to_permalink(string, *args)
        - string_to_escapedhtml(string)
        - string_to_tags(string)
        - get_new_dynamic_key()
    """
    # Class
    def __setitem__(self, key, value):
        # TODO: what should happen to the order if
        #       the key is already in the dict
        self.itemlist.append(key)
        super(RequestHandler, self).__setitem__(key, value)

    def __iter__(self):
        return iter(self.itemlist)

    def get_self(self):
        return self

    def keys(self):
        return self.itemlist

    def values(self):
        return [self[key] for key in dir(self)]

    def itervalues(self):
        return (self[key] for key in dir(self))

    # Path
    @staticmethod
    def path(*paths):
        # TODO supportare anche le liste come argument
        p = os.path.join(*paths)

        """ Returns absolute path """
        if os.path.isabs(p):
            return p

        return os.path.abspath(p)

    # Log
    def log(self, text):
        if self.logging:
            self.logging.debug(text)
        else:
            logging.debug(text)

    def l_log(self, *args, **kwargs):
        if self.logging:
            self.logging.log(*args)
        else:
            logging.log(*args)

    def d_log(self, text):
        if self.logging:
            self.logging.debug(text)
        else:
            logging.debug(text)

    def i_log(self, text):
        if self.logging:
            self.logging.info(text)
        else:
            logging.info(text)

    def w_log(self, text):
        if self.logging:
            self.logging.warn(text)
        else:
            logging.warn(text)

    def e_log(self, text):
        if self.logging:
            self.logging.error(text)
        else:
            logging.error(text)

    def f_log(self, text):
        if self.logging:
            self.logging.fatal(text)
        else:
            logging.fatal(text)

    def c_log(self, text):
        if self.logging:
            self.logging.critical(text)
        else:
            logging.critical(text)

    def exc_log(self, text):
        if self.logging:
            self.logging.exception(text)
        else:
            logging.exception(text)

    # Time
    @staticmethod
    def timestamp_now():
        # @todo Get Timestamp
        return str(datetime.datetime.now())

    @staticmethod
    def timestamp(date=datetime.datetime.now()):
        return time.time(date)

    # String and Datatypes
    @staticmethod
    def new_dynamic_key():
        return 'gbkit-%s' + str(random.randint(0, 100000))

    @staticmethod
    def unicode_to_string(data):
        import collections
        if isinstance(data, unicode):
            return str(data)
        elif isinstance(data, collections.Mapping):
            return dict(map(RequestHandler.unicode_to_string, data.iteritems()))
        elif isinstance(data, collections.Iterable):
            return type(data)(map(RequestHandler.unicode_to_string, data))
        else:
            return data

    @staticmethod
    def string_to_permalink(text):
        return re.sub('[-\s]+', '-', re.sub('[^\w\s-]', '', text.strip().lower()))

    @staticmethod
    def string_to_escapedhtml(text):
        """Escape text for use as HTML"""
        return cgi.escape(text, True).replace("'", '&#39;').encode('ascii', 'xmlcharrefreplace')

    @staticmethod
    def string_to_tags(text):
        words = text.replace('\n', ' ').strip().split(' ')
        singlewords = {}
        for word in words:
            singlewords.setdefault(word, singlewords.get(word, 1) + 1)

        words = []
        for word in singlewords:
            words.append({'word': word, 'count': singlewords[word]})
        words.sort(key=operator.itemgetter("count"))
        c = 0
        for word in words:
            if c <= 7:
                yield word.get('word')

    # ------------------------------------------------------------------ #
    #                          UTILITIES ENDS                            #
    # ------------------------------------------------------------------ #
    def serialize(self, value, rtype='json'):
        output = None
        try:
            exec('output = formatter.%s.dumps_s(value)'\
             % rtype)
        except:
            pass
        return output

    def unserialize(self, value, rtype='json'):
        output = None
        try:
            exec('output = formatter.%s.loads(value)'\
             % rtype)
        except Exception, e:
            output = e.message
            pass
        return output
    """
    _history = []       # All settings
    _meta = {}          # Meta settings
    _meta_list = []     # Meta settings list
    _robots = {}        # Robots settings
    _robots_list = []   # Robots settings list
    _settings = gbkit_config.SETTINGS_DEFAULT
    _settingsExclusionsPrefix = gbkit_config.SETTINGS_EXCLUSIONS_PREFIXES

    def get_query(self, kind=False, namespace=False, sort=False, conditions=False, limit=1000, offset=0):
        if not kind:
            raise Exception('get_query: kind arg not specified')

        query = 'SELECT * FROM ' + (kind or namespace)

        if conditions:
            query += 'WHERE ' + (' AND '.join(conditions))
        if sort:
            query += 'ORDER BY ' + (', '.join(sort))

        query += 'LIMIT ' + str(offset) + ', ' + str(limit)

        return google.appengine.ext.db.GqlQuery(query)

    def get_keys_query(self, kind=False, namespace=False, sort=False, conditions=False, limit=1000, offset=0):
        if not kind:
            raise Exception('get_query: kind arg not specified')

        query = 'SELECT __key__ FROM ' + (kind or namespace)

        if conditions:
            query += 'WHERE ' + (' AND '.join(conditions))
        if sort:
            query += 'ORDER BY ' + (', '.join(sort))

        query += 'LIMIT ' + str(offset) + ', ' + str(limit)

        return google.appengine.ext.db.GqlQuery(query)

    def get_data(self, kind=None):
        return
        if kind == 'settings' and kind not in self._data:
            from application.models.settings import Settings
            options = Settings.all().fetch(1000)
            self._data.setdefault(kind, options)

            for option in options:
                if option.field == 'theme':
                    self._theme = option.value

                if option.field not in self._settingsMetaInclusion and option.field[:4] == 'meta':
                    self._meta.setdefault(option.field[5:], option.value)
                    self._meta_list.append({'name': option.field[5:], 'value': option.value})

                if option.field[:6] == 'robots':
                    self._robots.setdefault(option.field[7:], option.value)
                    self._robots_list.append({'name': option.field[7:], 'value': option.value})

                else:
                    self._settings.setdefault(option.field, option.value)

            self.log("[GBKit] Settings loaded.")

        if kind == 'pages' and kind not in self._data:
            from application.models.pages import Page
            self._data.setdefault(kind, Page.all().fetch(1000))
            self.log("[GBKit] Pages loaded.")

        if kind == 'history' and kind not in self._data:
            from application.models.history import SettingsHistory
            self._data.setdefault(kind, SettingsHistory.all().fetch(1000) or [])
            self.log("[GBKit] History loaded.")

        if kind:
            return self._data.get(kind, [])

    def settings_meta_inclusion_list(self):
        return self._settingsMetaInclusion

    def settings_data(self):
        return self.get_data('settings')

    def settings(self):
        self.get_data('settings')
        return self._settings

    def robots(self):
        self.get_data('settings')
        return self._robots

    def robots_data(self):
        self.get_data('settings')
        return self._robots_list

    def meta(self):
        self.get_data('settings')
        return self._meta

    def meta_data(self):
        self.get_data('settings')
        return self._meta_list

    def location(self):
        if self.settings().get('url'):
            return {
                'uri': self.request.uri,
                'url': self.settings().get('url') + self.request.uri,
                'domain': self._settings.get('url').split('/')[2],
                'port': self._settings.get('url'),
                'user_ip': os.environ.get('REMOTE_ADDR', ''),
                'file': self.request.uri.split('/')[-1]
            }

        return {
            'uri': self.request.uri,
            'url': 'http://' + os.environ.get('HTTP_HOST', '') + self.request.uri,
            'domain': os.environ.get('SERVER_NAME', ''),
            'port': os.environ.get('SERVER_PORT', ''),
            'user_ip': os.environ.get('REMOTE_ADDR', ''),
            'file': self.request.uri.split('/')[-1].split('?')[0]
        }
"""


class APIRequestHandler(RequestHandler):
    _requested_format = 'json'
    api_mode = True
    api_httpcodes_enabled = True
    forced_secure_encoding = True
    debug_on_headers = False
    forced_get_requests_lock = False

    def initialize(self, request, response):
        super(APIRequestHandler, self).initialize(request, response)

        sort_rules = self.unserialize(request.get('sort', default_value="[]"))
        filters = self.unserialize(request.get('filter', default_value="[]"))
        path = request.uri.split('?')[0].split('/')
        action = path[7] if len(path) >= 8 else None
        params = path[7:][1:] if len(path) >= 8 else None
        if request.headers.get('Content-Type') is not None:
            json_enabled = 'json' in request.headers.get('Content-Type')
        else:
            json_enabled = False
        self._crud_api_request = {
            'limit': int(request.get('limit', default_value=1000)),
            'start': int(request.get('start', default_value=0)),
            'method': request.method,
            'uri': request.uri,
            'success': True,
            'filters': filters,
            'sort': sort_rules,
            'request': {
                'filters': filters,
                'sort': sort_rules,
                'json': json_enabled,
                'area': path[3],
                'type': path[4],
                'block': path[5],
                'model': path[6],
                'action': action,
                'params': params,
                'args': request.arguments(),
                'args_string': request.GET,
                'handler': None
            }
        }

    def error(self, *args, **kwargs):
        self._crud_api_request['success'] = False
        super(APIRequestHandler, self).error(*args, **kwargs)

    def set_development_environment(self):
        #self.forced_factories_output = True
        self.forced_user_output = True
        self.cache_enabled = False
        self.csrf_protection_enabled = False

    def set_production_environment(self):
        self.forced_user_output = True
        self.cache_enabled = True
        self.csrf_protection_enabled = True

    def init_environment_check(self):
        super(APIRequestHandler, self).init_environment_check()
        if self.environment.is_development:
            self.set_development_environment()
        else:
            self.set_production_environment()


class CRUDAPIRequestHandler(APIRequestHandler):
    def initialize(self, request, response):
        super(CRUDAPIRequestHandler, self).initialize(request, response)
        models = self._gbkit_settings.DATASTORE_API_MODELS
        try:
            model = models[self._crud_api_request['request']['model']]
            exec("""from %s import %s
self._crud_api_request['request']['_handler'] = %s"""  \
        % (model[0], model[1], model[1]))
        except:
            self.raise_error('Model not found', 404)

    def get_template_vars(self, data=None, default=None):
        newdata = super(CRUDAPIRequestHandler, self).get_template_vars(data, default)
        ret = self._crud_api_request
        if '_handler' in ret['request']:
            del ret['request']['_handler']
        newdata['crud_api_request'] = ret
        return newdata


class RESTAPIRequestHandler(APIRequestHandler):
    def post(self, key=None):
        pass

    def get(self, key=None):
        pass

    def put(self, key=None):
        pass

    def delete(self, key=None):
        pass

class RESTCRUDAPIRequestHandler(CRUDAPIRequestHandler):
    def post(self, key=None):
        pass

    def get(self, key=None):
        pass

    def put(self, key=None):
        pass

    def delete(self, key=None):
        pass