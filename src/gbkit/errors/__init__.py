#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import os
import gbkit_config
import sys
import threading
import cStringIO
import traceback
import logging
from gbkit.datatypes import Object

VALID_ATTRIBUTES = [
    'errno', 'code', 'strerror', 'msg', 'message', 'details'
]

CODE_ATTRIBUTES = [
    'code', 'errno'
]

TEXT_ATTRIBUTES = [
    'msg', 'message', 'strerror', 'title'
]

DETAILS_ATTRIBUTES = [
    'explanation'
]

class Error(Object):
# TODO Rimuovere le ultime 3 dallo stack trace
# output html
# output string
# __json__
# __dict__
# __str__
#
    request_id = None
    request_id_hash = None
    request_log_id = None
    filename = None
    row = None
    parent = None
    rowcontent = None
    line = None
    text = None
    message = None
    classname = None
    code = 0
    thread = None
    details = None
    string_traceback = None
    string_stack = None

    exception = None
    tb = None
    traceback = None
    stack = None

    def __init__(self, code=0, text=None, details=None,
        exception=None, log=True):  # gbkit_config.ENVIRONMENT_DEBUG):
        self.request_id = os.environ.get('REQUEST_ID', 'unknown')
        self.request_id_hash = os.environ.get('REQUEST_ID_HASH', 'unknown')
        self.request_log_id = os.environ.get('REQUEST_LOG_ID', 'unknown')
        self.thread = threading.current_thread().name
        self.traceback = cStringIO.StringIO()
        self.stack = cStringIO.StringIO()
        self.message = text
        if text is None and exception is str:
            self.text = exception
        if exception is str:
            self.exc_message = exception
        elif exception:
            ex_stack = traceback.extract_stack()
            row = ex_stack[-4]
            self.filename = "'%s'" % row[0]
            self.line = "%s" % row[1]
            self.parent = "'%s'" % row[2]
            self.row = "'%s'" % row[3]

            self.exception = exception
            #self.text = str(exception)
            for key in VALID_ATTRIBUTES:
                if key in CODE_ATTRIBUTES and hasattr(exception, key):
                    self.code = getattr(exception, key)
                if key in TEXT_ATTRIBUTES and self.text is None \
                and hasattr(exception, key):
                    self.text = getattr(exception, key)
                if key in DETAILS_ATTRIBUTES and hasattr(exception, key):
                    self.details = getattr(exception, key)
                elif hasattr(exception, key):
                    setattr(self, '_%s' % key, getattr(exception, key))

        if sys.exc_info()[0] is not None and sys.exc_info()[1] is not None:
            exc_type, exc_value, exc_tb = sys.exc_info()[:3]

            ex_trace = traceback.extract_tb(exc_tb)
            row = ex_trace[-1]
            #self.filename += " - '%s'" % \
            #    str(os.path.sep.join(row[0].split(os.path.sep)[-4:]))
            #self.line = "%s - %s" % \
            #    (str(self.line), row[1])
            #if len(row) >= 2:
            #    self.parent += " - '%s'" % \
            #        str(row[2])
            #    if len(row) >= 4:
            #        self.row += " - '%s'" % \
            #            str(row[3])

            self.classname = exc_type.__name__
            #self.text = exc_value
            self.tb = exc_tb
            traceback.print_exception(exc_type, exc_value, exc_tb, file=self.traceback)
            self.string_traceback = self.traceback.getvalue()
        traceback.print_stack(file=self.stack)
        self.string_stack = self.stack.getvalue()

        if code:
            self.code = int(code)
        #if text:
            #self.text = text
        if not self.text and self.code in gbkit_config.HTTP_STATUSES:
            self.text = gbkit_config.HTTP_STATUSES.get(self.code)
        if not self.text and self.code in gbkit_config.HTTP_STATUSES:
            self.text = gbkit_config.HTTP_STATUSES.get(self.code)
        self.log()

    def log(self):
        import google.appengine.api.urlfetch
        import google.appengine.api.runtime
        import google.appengine.api.taskqueue
        if isinstance(self.exception, google.appengine.api.urlfetch.DownloadError) or isinstance(self.exception, google.appengine.runtime.DeadlineExceededError) or isinstance(self.exception, google.appengine.api.taskqueue.TransientError):
            logger = logging.warn
        else:
            logger = logging.error
        logger('[GBKit][EXCEPTION] %s' % ('-' * 16))
        logger('[GBKit][EXCEPTION] Exception caught!')
        if self.request_id is not None:
            logger('[GBKit][EXCEPTION] Request Id: %s' % self.request_id)
        if self.request_id_hash is not None:
            logger('[GBKit][EXCEPTION] Request Id Hash: %s' % self.request_id_hash)
        if self.request_log_id is not None:
            logger('[GBKit][EXCEPTION] Log Id: %s' % self.request_log_id)
        if self.filename is not None:
            logger('[GBKit][EXCEPTION] Filename: %s' % \
                self.filename.replace(gbkit_config.BASEPATH, ''))
        if self.line is not None:
            logger('[GBKit][EXCEPTION] Line: %s' % self.line)
        if self.parent is not None:
            logger('[GBKit][EXCEPTION] Parent: %s' % self.parent)
        if self.row is not None:
            logger('[GBKit][EXCEPTION] Row: %s' % self.row)
        if self.text is not None:
            logger('[GBKit][EXCEPTION] Message "%s"' % self.text)
        if self.classname is not None:
            logger('[GBKit][EXCEPTION] Handling %s exception' % \
                self.classname)
        if int(self.code):
            logger('[GBKit][EXCEPTION] Code: %i' % self.code)
        if self.thread is not None:
            logger('[GBKit][EXCEPTION] Thread: %s' % self.thread)
        if self.details is not None:
            logger('[GBKit][EXCEPTION] Details \n%s\n' % \
                self.details)
        if self.traceback is not None:
            logger('[GBKit][EXCEPTION] Trace \n%s\n' % \
                self.string_traceback)
        if self.stack is not None:
            logger('[GBKit][EXCEPTION] Stack \n%s\n' %
                self.string_stack)
        logger('[GBKit][EXCEPTION] %s' % ('-' * 16))

    def get_html(self):
        ret = "<h3>Error Informations</h3>"
        if self.request_id is not None:
            ret += """<b>Request Id</b> <i>%s</i><br />""" % \
            self.request_id
        if self.request_id_hash is not None:
            ret += """<b>Request Id Hash</b> <i>%s</i><br />""" % \
            self.request_id_hash
        if self.request_log_id is not None:
            ret += """<b>Log Id</b> <i style="color: red;">%s</i><br />""" \
                % self.request_log_id
        if self.filename is not None:
            ret += """<b>Filename</b> <b style="color: red;">%s</b><br />""" % \
            self.filename.replace(gbkit_config.BASEPATH, '')
        if self.line is not None:
            ret += """<b>Line</b> <b style="color: red;">%s</b><br />""" % self.line
        if self.parent is not None:
            ret += """<b>Parent</b> %s<br />""" % self.parent
        if self.row is not None:
            ret += """<b>Row</b> %s<br />""" % self.row
        if self.text is not None:
            ret += """<b>Message</b> <b style="color: red;">%s</b><br />""" % self.text
        if self.classname is not None:
            ret += """<b>Exception</b> %s<br /><br />""" % str(self.classname)
        if bool(int(self.code)):
            ret += """<b>Code</b> %s<br />""" % str(self.code)
        if self.thread is not None:
            ret += """<b>Thread</b> %s<br />""" % self.thread
        if self.details is not None:
            ret += """<b>Details</b> %s<br />""" % self.details
        if self.string_traceback is not None:
            ret += """<b>Last Working</b><pre>%s</pre>""" % self.string_traceback
        if self.string_stack is not None:
            ret += """<b>Full Stack</b><pre>%s</pre>""" % self.string_stack
        return ret

    def __json__(self):
        return {
            'request_id': self.request_id,
            'request_id_hash': self.request_id_hash,
            'request_log_id': self.request_log_id,
            'filename': self.filename,
            'line': self.line,
            'parent': self.parent,
            'row': self.row,
            'exception': self.exception,
            'classname': self.classname,
            'message': self.message,
            'text': self.text,
            'code': self.code,
            'thread': self.thread,
            'details': self.details,
            'string_traceback': self.string_traceback,
            'string_stack': self.string_stack,
            'get_html': self.get_html()
        }

    def __dict__(self):
        return {
            'request_id': self.request_id,
            'request_id_hash': self.request_id_hash,
            'request_log_id': self.request_log_id,
            'filename': self.filename,
            'line': self.line,
            'message': self.message,
            'text': self.text,
            'parent': self.parent,
            'row': self.row,
            'exception': self.exception,
            'classname': self.classname,
            'code': self.code,
            'thread': self.thread,
            'details': self.details,
            'string_traceback': self.string_traceback,
            'string_stack': self.string_stack,
            'get_html': self.get_html()
        }
