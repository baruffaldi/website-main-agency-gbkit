#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import gbkit
from gbkit.wsgi import WSGIApplication
from gbkit.wsgi.handlers import RequestHandler, DEBUG, wsgi_config
from google.appengine.ext.webapp.util import login_required

REDIRECTS = {
    'login': '/gbkit/users/login',
    'logout': '/gbkit/users/logout'
}


class Favicon(RequestHandler):
    __metaclass__ = gbkit.LoggerDisabledMetaClass

    def get(self):
        # Cache fetch
        if self.cache_enabled:
            image = self.store.memcache.get("gbkit_favicon")
            mimetype = self.store.memcache.get("gbkit_favicon_mimetype")
            if image is not None:
                self.response.headers["Content-Type"] = str(mimetype)
                self.response.out.write(image)
                self.log('[GBKit] cached favicon served.')
                return

        if False:  # Settings.all(keys_only=True).filter('field =', 'favicon').count(1) and  Settings.all(keys_only=True).filter('field =', 'favicon_mimetype').count(1):
            import base64
            mimetype = self.settings_factory.all().filter('field =', 'favicon_mimetype').fetch(1)[0].value
            self.response.headers["Content-Type"] = str(mimetype)

            image = base64.b64decode(self.settings_factory.all().filter('field =', 'favicon').fetch(1)[0].value)
            self.response.out.write(image)
            self.log('[GBKit] logo served.')

        # Cache store
        if self.cache_enabled and image and mimetype:
            self.store.memcache.add_multi({"gbkit_favicon": image, "gbkit_favicon_mimetype": mimetype}, 600000)

        self.log('[GBKit] gbkit favicon served.')
        self.redirect('/gbkit/asset/images/favicon.png', True)


class Logo(RequestHandler):
    __metaclass__ = gbkit.LoggerDisabledMetaClass

    def get(self):
        # Cache fetch
        if self.cache_enabled:
            image = self.store.memcache.get("gbkit_logo")
            mimetype = self.store.memcache.get("gbkit_logo_mimetype")
            if image is not None:
                self.response.headers["Content-Type"] = str(mimetype)
                self.response.out.write(image)
                self.log('[GBKit] cached logo served.')
                return

        if False:  # Settings.all(keys_only=True).filter('field =', 'logo').count(1) and  Settings.all(keys_only=True).filter('field =', 'logo_mimetype').count(1):
            import base64
            mimetype = self.settings_factory.all().filter('field =', 'logo_mimetype').fetch(1)[0].value
            self.response.headers["Content-Type"] = str(mimetype)

            image = base64.b64decode(self.settings_factory.all().filter('field =', 'logo').fetch(1)[0].value)
            self.response.out.write(image)
            self.log('[GBKit] logo served.')

        # Cache store
        if self.cache_enabled and image and mimetype:
            self.store.memcache.add_multi({"gbkit_logo": image, "gbkit_logo_mimetype": mimetype}, 600000)

        self.log('[GBKit] gbkit logo served.')
        self.redirect('/gbkit/asset/images/logo.png', True)


class AuthSubHTMLPage(RequestHandler):
    __metaclass__ = gbkit.LoggerDisabledMetaClass

    """Simulates an HTML page to prove ownership of this domain for AuthSub
    registration."""
    def get(self, id="0"):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write('google-site-verification: google%s.html' % id)


class Robots(RequestHandler):
    def get(self):
        robots = "Sitemap: /sitemap.xml.gz\n" \
                 "Sitemap: /sitemap.xml\n" \
                 "User-agent: *\n" \
                 "Disallow: /errors/\n" \
                 "Disallow: /admin/\n" \
                 "Disallow: /administration/\n" \
                 "Disallow: /_ah/\n"

        if self.settings_factory.all(keys_only=True).filter('field =', '').count(1):
            for option in self.settings_factory.all().filter('field =', '').fetch(1000):
                if option.value[:6]:
                    robots += option.field + ": " + option.value + "\n"

        self.response.headers["Content-Type"] = "text/plain"
        self.response.out.write(robots)


class Sitemap(RequestHandler):
    def get(self):
        ""
        if self.request.uri.split('.')[-1] == 'gz':
            "loadplugins=True, loadthemes=True"


class Index(RequestHandler):

    @login_required
    def get(self, *args, **kwargs):
        tpl = 'index.html'
        tplname = tpl.replace('.', '_')

        # Cache fetch
        if self.cache_enabled:
            page = self.store.memcache.get("gbkit_page_%s" % tplname)
            if page is not None:
                self.i_log('[GBKit] Cached page index.html served.')
                self.response.out.write(page)
                return

        page = self.render(self.get_template(tpl), ret=True)
        self.log('[GBKit] Page index.html served.')
        self.response.out.write(page)

        # Cache store
        if self.cache_enabled:
            self.store.memcache.add("gbkit_page_%s" % tplname, page, (60 * 60 * 3))


class NotFound(RequestHandler):
    def get(self):
        self.raise_error(code=404)


class Redirect(RequestHandler):
    def get(self, destination=None):
        if destination in REDIRECTS:
            self.redirect(
                REDIRECTS[destination],
                self.request.get('last', default_value=False))
        else:
            self.raise_error(code=404)


routes = [
    (r'/favicon\..*', Favicon),
    (r'/logo\..*', Logo),
    (r'/google([0-9a-f]+)\.html', AuthSubHTMLPage),
    (r'/robots\.txt', Robots),
    (r'/sitemap\.xml.*', Sitemap),

    (r'/_(.*)', Redirect),

    (r'/(|gbkit/)', Index),
    (r'.*', NotFound)
]
app = WSGIApplication(routes, debug=DEBUG, config=wsgi_config)
