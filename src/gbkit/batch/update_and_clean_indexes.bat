@echo off
set BASEURL="C:\Users\bornslippy\Google Drive\_eclipse_workspace\gpress-professional"
echo GPRESS-UPLOADER: Updating Indexes
appcfg.py update_indexes %BASEURL%\src
echo GPRESS-UPLOADER: Clear Unused Indexes
appcfg.py vacuum_indexes %BASEURL%\src
pause