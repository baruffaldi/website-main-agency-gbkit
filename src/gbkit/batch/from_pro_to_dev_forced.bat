@echo off
set BASEURL="C:\Users\bornslippy\Google Drive\_eclipse_workspace\gpress-professional"
set KINDS=Settings,Address,Page,PageCategory,PageDetails
rem set KINDS=Settings,SettingsHistory,Address,Page,PageCategory
rem TODO: set ALL_KINDS=1
rem TODO: Visualizzazione Step sul numero di kinds da lavorare
set PROJECT_ID=gbc-main
set DEV_PROJECT_ID=%PROJECT_ID%
rem set DEV_PROJECT_ID=gbc-italy
set G_DEBUG=0

set URL=http://www.gbc-italy.com/_ah/remote_api
set DEV_URL=http://localhost:8085/_ah/remote_api
set NTHREADS=4 
for /f "tokens=1-2 delims=/:" %%a in ("%TIME%") do (set DATA=%%a%%b)
set APP_ID=s~%PROJECT_ID%
set DEV_APP_ID=dev~%DEV_PROJECT_ID%
set BULKYAML=%BASEURL%\bulkloader.yaml
set D_DIR=%BASEURL%\src
if not exist %BULKYAML% (
	echo GPRESS-UPLOADER: Creating bulkloader.yaml for application %APP_ID%
	appcfg.py --application="%APP_ID%" --filename="%BULKYAML%" --url="%URL%" create_bulkloader_config %D_DIR%
	if %G_DEBUG% == 1 pause
)

call :parse "%KINDS%"
goto :end

:parse
setlocal
set list=%1
set list=%list:"=%
FOR /f "tokens=1* delims=," %%a IN ("%list%") DO (
  if not "%%a" == "" call :sub %%a
  if not "%%b" == "" call :parse "%%b"
)
endlocal
exit /b

:sub
setlocal
	set FILENAME=%BASEURL%\data\data.pro.%PROJECT_ID%.%1.%DATA%.sqlite
	if not exist %FILENAME% (
		echo GPRESS-UPLOADER: Downloading %1 data from production application %APP_ID%
		appcfg.py --kind="%1" --application="%APP_ID%" --config_file="%BULKYAML%" --filename="%FILENAME%" --url="%URL%" download_data %D_DIR%
		if %G_DEBUG% == 1 pause
	)
	if exist %BULKYAML% (
		if exist %FILENAME% (
			echo GPRESS-UPLOADER: Uploading %APP_ID% %1 data to development server 
			appcfg.py --kind="%1" --num_threads=%NTHREADS% --config_file="%BULKYAML%" --application="%DEV_APP_ID%" --filename="%FILENAME%" --url="%DEV_URL%" upload_data %D_DIR%
			if %G_DEBUG% == 1 pause
		)
	)
endlocal
exit /b

:end
echo Finished!
pause
echo Closing...