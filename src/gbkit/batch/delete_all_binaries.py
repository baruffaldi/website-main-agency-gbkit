import os

BASEPATH = os.path.abspath(
    os.path.join(
        os.path.split(__file__)[0],
        '..', '..'
))
print BASEPATH

def chunk_reader(fobj, chunk_size=1024):
    """Generator that reads a file in chunks of bytes"""
    while True:
        chunk = fobj.read(chunk_size)
        if not chunk:
            return
        yield chunk


def check_for_duplicates(path):
    for dirpath, dirnames, filenames in os.walk(path):
        for filename in filenames:
            if filename.endswith('.pyc'):
                print "Removed %s" % filename
                os.remove(os.path.join(dirpath, filename))
            #else:
            #    print "Skipped %s" % filename

check_for_duplicates(BASEPATH)
