#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Google AppEngine File Handler
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: Google AppEngine File Handler
@license: Apache 2.0
@url: http://www.gbc-italy.com
@version: 1
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "Google AppEngine File Handler"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

from StringIO import StringIO

from django.template.loader import get_template



class File(StringIO):
    closed = False
    buflist = []
    len = 0
    buf = ""
    pos = 0
    """"""

    def __init__(self, path=None):
        self.buf = get_template(path)
        self.buflist = self.buf.split('\n')
        self.len = len(self.buf)


def fopen(path):
    return File(path)


def fclose(stream):
    stream.close()
