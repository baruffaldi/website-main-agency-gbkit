#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

from google.appengine.ext import db
from gbkit.wsgi import WSGIApplication
from gbkit.wsgi.handlers import CRUDAPIRequestHandler, RESTCRUDAPIRequestHandler, DEBUG, wsgi_config
from google.appengine.ext.webapp.util import login_required


class Get(CRUDAPIRequestHandler):
    def post(self, key=None):
        model = self._crud_api_request['request']['_handler']
        if key is None:
            key = self.request.get('key', default_value=None)
        if key is None:
            query = model.all()
            if self._crud_api_request['sort'] != []:
                for sort in self._crud_api_request['sort']:
                    query.order("%s%s" % ('-' if sort['direction'].lower() == 'desc' else '', sort['property'] if sort['property'] != 'key' else '__key__'))
            entries = query.fetch(
                self._crud_api_request['limit'],
                self._crud_api_request['start'])
        else:
            try:
                entries = [model.get(key)]
            except:
                self.error(404)
                return
        self.render(None, entries)


class List(CRUDAPIRequestHandler):
    def post(self):
        model = self._crud_api_request['request']['_handler']
        query = model.all()
        if self._crud_api_request['sort'] != []:
            for sort in self._crud_api_request['sort']:
                query.order("%s%s" % ('-' if sort['direction'].lower() == 'desc' else '', sort['property'] if sort['property'] != 'key' else '__key__'))
        entries = query.fetch(
            self._crud_api_request['limit'],
            self._crud_api_request['start'])
        self.render(None, entries)


class Update(CRUDAPIRequestHandler):
    def post(self, action=None):
        model = self._crud_api_request['request']['_handler']
        if action not in ['update', 'insert', 'new', 'create', 'add']:
            self.raise_error('Datastore API Method not found', 405)
        entity_parameters = {}
        if self._crud_api_request['request']['json']:
            tmp_entity = self.formatter.json.loads(str(self.request).split('\n').pop().strip())
            for field in tmp_entity.keys():
                if str(field) not in model._protected and \
                    str(field) not in model._collections and \
                    str(field) not in model._collections_single_reference:
                    entity_parameters[field] = tmp_entity[field]
        else:
            for field in self.request_vars().keys():
                if self.request.get(field, default_value=False) and \
                    str(field) not in model._protected and \
                    str(field) not in model._collections and \
                    str(field) not in model._collections_single_reference:
                    entity_parameters[field] = self.request.get(field)

        if action == 'update' and self.request.get('key', default_value=False):
            entry = model.get(self.request.get('key'))
            entry.intelligent_update(entity_parameters)
        else:
            entry = model(**entity_parameters)
        entry.save()
        self.render(None, entry)


class Delete(CRUDAPIRequestHandler):
    def post(self):
        entries_to_delete = self.request.get_all('key')
        db.delete([db.Key(x) for x in entries_to_delete])
        self.render(None, True)


class Rest(RESTCRUDAPIRequestHandler):
    @login_required
    def get(self, key=None):
        model = self._crud_api_request['request']['_handler']
        key = getattr(self._crud_api_request['filters'], 'key', None)
        if key is None:
            key = self.request.get('key', default_value=None)
        if key is None:
            key = self._crud_api_request['request']['action']
        if key is None:
            query = model.all()
            if self._crud_api_request['filters'] != []:
                for f in self._crud_api_request['filters']:
                    if hasattr(f, 'property') and f['property'] != 'key':
                        query.filter("%s =" % f['property'], f['value'])
            if self._crud_api_request['sort'] != []:
                for sort in self._crud_api_request['sort']:
                    query.order("%s%s" % ('-' if sort['direction'].lower() == 'desc' else '', sort['property'] if sort['property'] != 'key' else '__key__'))
            entries = query.fetch(
                self._crud_api_request['limit'],
                self._crud_api_request['start'])
        else:
            try:
                entries = [db.get(key)]
            except:
                self.error(404)
                return
        self.render(None, entries)

    def post(self, key=None):
        model = self._crud_api_request['request']['_handler']
        entity_parameters = {}
        entry = None
        if self._crud_api_request['request']['json']:
            entity_parameters = self.formatter.json.loads(str(self.request).split('\n').pop().strip())
            """tmp_entity = self.formatter.json.loads(str(self.request).split('\n').pop().strip())
            for field in tmp_entity.keys():
                if str(field) not in model._protected and \
                    str(field) not in model._collections and \
                    str(field) not in model._collections_single_reference:
                    entity_parameters[field] = tmp_entity[field]"""
        else:
            for field in self.request_vars().keys():
                #if self.request.get(field, default_value=False) and \
                    #str(field) not in model._protected and \
                    #str(field) not in model._collections and \
                    #str(field) not in model._collections_single_reference and \
                    #self.request.get(field, default_value=False) != u'':
                    entity_parameters[field] = self.request.get(field)
        import logging
        logging.warn(entity_parameters)
        try:
            entry = model(**entity_parameters)
        except Exception, e:
            self.raise_error('Error: %s.' % e.message, 500)
        if entry is not None:
            new_entity = entry.intelligent_update(entity_parameters)
            for key in new_entity.keys():
                setattr(entry, key, new_entity[key])
            entry.put()
        self.render(None, entry)

    def put(self, key=None):
        #model = self._crud_api_request['request']['_handler']
        entity_parameters = {}
        if self._crud_api_request['request']['json']:
            entity_parameters = self.formatter.json.loads(str(self.request).split('\n').pop().strip())
            """tmp_entity = self.formatter.json.loads(str(self.request).split('\n').pop().strip())
            for field in tmp_entity.keys():
                if str(field) not in model._protected and \
                    str(field) not in model._collections and \
                    str(field) not in model._collections_single_reference:
                    entity_parameters[field] = tmp_entity[field]"""
        else:
            for field in self.request_vars().keys():
                #if self.request.get(field, default_value=False) and \
                    #str(field) not in model._protected and \
                    #str(field) not in model._collections and \
                    #str(field) not in model._collections_single_reference and \
                    #self.request.get(field, default_value=False) != u'':
                    entity_parameters[field] = self.request.get(field)

        if key is None:
            key = self.request.get('key', default_value=None)
        if key is None:
            key = self._crud_api_request['request']['action']
        if key is not None:
            entry = None
            try:
                entry = db.get(key)
            except:
                self.raise_error('Entity not found.', 404)
            if entry is not None:
                new_entity = entry.intelligent_update(entity_parameters)
                for key in new_entity.keys():
                    setattr(entry, key, new_entity[key])
                entry.put()
        else:
            self.raise_error('Entity not specified.', 404)
        self.render(None, entry)

    def delete(self, key=None):
        entries_to_delete = key.split('/')
        db.delete(entries_to_delete)
        self.render(None, True)


class NotFound(CRUDAPIRequestHandler):
    def get(self):
        self.raise_error('Datastore API Method not found', 405)


routes = [
    (r'/gbkit/crud/datastore/.*/list', List),
    (r'/gbkit/crud/datastore/.*/(update|modify|insert|new|create)', Update),
    (r'/gbkit/crud/datastore/.*/delete', Delete),
    (r'/gbkit/crud/datastore/.*/get', Get),
    (r'/gbkit/crud/datastore/.*/get/', Get),
    (r'/gbkit/crud/datastore/.*/get/(.*)', Get),
    (r'/gbkit/crud/datastore/.*/read', Get),
    (r'/gbkit/crud/datastore/.*/read/', Get),
    (r'/gbkit/crud/datastore/.*/read/(.*)', Get),
    (r'/gbkit/crud/datastore/.*/retrieve', Get),
    (r'/gbkit/crud/datastore/.*/retrieve/', Get),
    (r'/gbkit/crud/datastore/.*/retrieve/(.*)', Get),
    (r'/gbkit/rest/datastore/.*/(.*)', Rest),
    (r'/gbkit/rest/datastore/.*', Rest),
    (r'.*', NotFound)
]
app = WSGIApplication(routes, debug=DEBUG, config=wsgi_config)
