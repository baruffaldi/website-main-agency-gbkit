#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1

import webapp2

from gbkit.wsgi import WSGIApplication
from gbkit.wsgi.handlers import RequestHandler, DEBUG, wsgi_config
from gbkit import store

from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers.json import JSONLexer
from pygments.lexers import JavascriptLexer
from pygments.lexers import IniLexer
from pygments.lexers import YamlLexer
from pygments.lexers import HtmlLexer
from pygments.lexers import XmlLexer
from pygments.lexers import PythonLexer


class GBKitTestDataStoreModel(store.models.SerializableModel):
    counter = store.db.gaedb.IntegerProperty(required=True)

TEST_EXCEPTIONS = {
    'timeout': None,
    'dos_api_denial': None,
}

TEST_CODES = {
    'dev': ("""
        output = self.formatter.xml.dumps(self._wsgi_settings)
""", XmlLexer),

    'session': ("""
self.session['counter'] = 1 if 'counter' not in self.session else self.session['counter'] + 1
output = self.formatter.json.dumps(self.session)
""", JSONLexer),

    'cookies': ("""
value = self.cookies.get('gbkit-counter', default="1")
self.cookies.set('gbkit-counter', int(value)+1)
o.append(self.formatter.json.dumps(int(value)+1))

value = self.cookies['gbkit-counter2'] \\
    if self.cookies['gbkit-counter2'] else "1"
self.cookies['gbkit-counter2'] = float(value) + 2.7
o.append(self.formatter.json.dumps(float(value) + 2.7))
""", JSONLexer),

    'memcache': ("""
value = self.memcache['counter']
value = 1 if value is None else value + 1
self.memcache['counter'] = value
output = self.formatter.json.dumps(value)

value = self.memcache.get('counter')
value += 1
self.memcache.set('counter', value)
output += "\\n%s" % self.formatter.json.dumps(value)
""", JSONLexer),

    'datastore': ("""
entity = GBKitTestDataStoreModel(counter=0)
key = entity.put()
o.append("put entity\\n- key: %s" % key)
entity = GBKitTestDataStoreModel.get(key)
o.append("get_entity\\n- record found: %s\\n- counter: %i" % (str(bool(entity)), entity.counter))
entity.counter += 3
entity.put()
o.append("update value\\n- counter: %i" % entity.counter)
entity.delete()
o.append("delete entity\\n- deleted entity: %s" % (not bool(GBKitTestDataStoreModel.get(key))))
""", JSONLexer),

    'registry': ("""
self.set_registry_item('test', 'blah')
self.set_registry_item('test2', 345)
app = webapp2.get_app()
output = self.formatter.json.dumps_s(app.registry)  # dumps_s is for safe dump
""", JSONLexer),

    'request': ("""
output = self.formatter.json.dumps_s(self._wsgi_settings)  # dumps_s is for safe dump
""", JSONLexer),

    'environment': ("""
output = self.formatter.json.dumps_s(self.environment)  # dumps_s is for safe dump
""", JSONLexer),

    'capabilities': ("""
output = self.formatter.json.dumps(self._services)
output += "\\n" + self.formatter.json.dumps(self._services_disabled)
""", JSONLexer),

    'pythonizer': ("""
output = str(self.formatter.pythonizer.encode(self._wsgi_settings))
""", PythonLexer),

    'json': ("""
output = str(self.formatter.json.loads(self.formatter.json.dumps_s(self._wsgi_settings)))
output += '\\n' + self.formatter.json.dumps_s(self._wsgi_settings)  # dumps_s is for safe dump
""", JSONLexer),

    'jsonp': ("""
output = str(self.formatter.jsonp.loads(self.formatter.jsonp.dumps_s(self._wsgi_settings)))
output += '\\n' + self.formatter.jsonp.dumps_s(self._wsgi_settings)  # dumps_s is for safe dump
""", JavascriptLexer),

    'jsonpickle': ("""
output = str(self.formatter.jsonpickle.loads(self.formatter.jsonpickle.dumps_s(self._wsgi_settings)))
output += '\\n' + self.formatter.jsonpickle.dumps(self._wsgi_settings)  # dumps_s is for safe dump
""", JSONLexer),

    'ubjson': ("""
output = str(self.formatter.ubjson.loads(self.formatter.ubjson.dumps_s(self._wsgi_settings)))
output += '\\n' + self.formatter.ubjson.dumps_s(self._wsgi_settings)  # dumps_s is for safe dump
""", None),

    'htmljsonp': ("""
output = str(self.formatter.htmljsonp.loads(self.formatter.htmljsonp.dumps_s(self._wsgi_settings)))
output += '\\n' + self.formatter.htmljsonp.dumps_s(self._wsgi_settings)  # dumps_s is for safe dump
""", HtmlLexer),

    'yaml': ("""
output = str(self.formatter.yaml.loads(self.formatter.yaml.dumps_s(self._wsgi_settings)))
output += '\\n' + self.formatter.yaml.dumps_s(self._wsgi_settings)  # dumps_s is for safe dump
""", YamlLexer),

    'pickle': ("""
output = str(self.formatter.pickle.loads(self.formatter.pickle.dumps_s(self._wsgi_settings)))
output += '\\n' + self.formatter.pickle.dumps_s(self._wsgi_settings)  # dumps_s is for safe dump
""", IniLexer),

    'xml': ("""
output = self.formatter.xml.dumps_s(self._wsgi_settings)  # dumps_s is for safe dump
""", XmlLexer),

    'csv': ("""
output = self.formatter.csv.dumps_s(self._wsgi_settings)  # dumps_s is for safe dump
""", IniLexer),

    'ini': ("""
output = self.formatter.ini.dumps_s(self._wsgi_settings)  # dumps_s is for safe dump
""", IniLexer)
}


class TestTemplateEngine(RequestHandler):
    def get(self):
        # TODO possibilità di aggiungere data per tpl su get
        # rendere dinamiche le configurazioni yaml usando il tpl render
        # possibilità per debug di visualizzare il risultato sia su console
        # che su html
        self.render(
            self.get_template(self.request.get('tpl', default_value='index.html')))


class TestCode(RequestHandler):
    def get(self, codename=None):
        if codename:
            self.response.out.write("""<html><head><link href="/gbkit/asset/stylesheets/pygments.css" rel="stylesheet" media="all" /><style>.err{border: none;color:#4070A0;}</style></head><body>""")

            o = []
            output = ""
            try:
                code, lexer = TEST_CODES[codename]
            except:
                self.error(404)
                self.response.out.write('GBKit Error: code not found.')
                return
            exec(code)
            self.response.out.write("<h2 style='margin:0 0 10px 0;color:#36C;padding:3px;width:100%;border-bottom: 1px solid #36C;background-color: #E5ECF9;'>Python Code on Request Handler</h2><pre style='border: 3px solid #36C;padding: 0 10px;margin: 0;'>")
            self.response.out.write(highlight(code, PythonLexer(), HtmlFormatter()))
            self.response.out.write("</pre><br /><h2 style='margin:0 0 10px 0;color:#36C;padding:3px;width:100%;border-bottom: 1px solid #36C;background-color: #E5ECF9;'>Output</h2><pre style='border: 3px solid #E5ECF9;padding: 0 10px;margin: 0;'>")
            if lexer:
                self.response.out.write(highlight(output if not o else "\n".join(o), lexer(),
                    HtmlFormatter()))
            else:
                self.response.out.write(str(output).replace(', u\'', ',\n u\''))
            self.response.out.write('</pre></body></html>')


class Index(RequestHandler):
    def get(self, *args, **kwargs):
        self.render(self.get_template('index-tests.html'))


class DumpTemplateContext(RequestHandler):
    def get(self, *args, **kwargs):
        self.render(self.get_template('dump_template_context.html'))


class TestTimeout(RequestHandler):
    def get(self):
        pass
        """
import google.appengine.runtime
import google.appengine.ext.db

EXCEPTIONS = {
    'envDeadline': google.appengine.runtime.DeadlineExceededError,
    'dbTimeout': google.appengine.ext.db.Timeout,
    'db500': google.appengine.ext.db.InternalError,
    'dbIndexMissing': google.appengine.ext.db.NeedIndexError,
    'dbNotSaved': google.appengine.ext.db.NotSavedError,
    #'sDisabled': google.appengine.ext.db.CapabilityDisabledError
}

        #raise Timeout
The google.appengine.api.backends package provides the following exception classes:

exception DefaultHostnameError()
Raised if no default hostname is set in the environment.

exception InvalidBackendError()
Raised if an invalid backend name was provided.

exception InvalidInstanceError()
Raised if an invalid instance parameter was provided.



The google.appengine.api.users package provides the following exception classes:

exception Error()
This is the base class for all exceptions in this package.

exception UserNotFoundError()
Raised by the User constructor if no email_address is provided and the current user is not logged in.

exception RedirectTooLongError()
The redirect URL given to create_login_url() or create_logout_url() exceeds the maximum allowed length for a redirect URL.

exception NotAllowedError()
The federated_identity parameter was given to create_login_url(), but Federeated Login is not set. If you are getting this error you may need to change the Authentication Options to Federated Login in the Application Settings.



The google.appengine.api.urlfetch package provides the following exception classes:

exception Error()
This is the base class for all exceptions in this package.

exception InvalidURLError()
The URL of the request was not a valid URL, or it used an unsupported method. Only http and https URLs are supported.

exception DownloadError()
There was an error retrieving the data.

This exception is not raised if the server returns an HTTP error code: In that case, the response data comes back intact, including the error code.

exception ResponseTooLargeError()
The response data exceeded the maximum allowed size, and the allow_truncated parameter passed to fetch() was False.




The google.appengine.api.images package provides the following exception classes:

exception Error()
This is the base class for all exceptions in this package.

exception BadImageError()
The given image data is corrupt.

exception BadRequestError()
The transformation parameters are invalid.

exception BlobKeyRequiredError()
A blob key is required for this operation.

exception LargeImageError()
The given image data is too large to process.

exception NotImageError()
The given image data is not in a recognized image format.

exception TransformationError()
There was an error while trying to transform the image.

exception UnsupportedSizeError()
The specified image size is not supported. For resized images, you can find supported image sizes in the IMAGE_SERVING_SIZES integer list. For cropped images, supported image sizes are available in the IMAGE_SERVING_CROP_SIZES integer list



The google.appengine.api.app_identity package provides the following exception classes:

exception BackendDeadlineExceeded()
Raised when the communication to the backend service times out.

exception BlobSizeTooLarge()
Raised when the size of the blob to sign is larger than the allowed limit.

exception Error()
This is the base class for all exceptions in this package.

exception InternalError()
Raised when an unspecified internal failure occurs.



The google.appengine.api.mail package provides the following exception classes:

exception Error()
This is the base class for all exceptions in this package.

exception BadRequestError()
The Mail service rejected the EmailMessage as invalid, and no other error applies.

exception InvalidSenderError()
The EmailMessage has a sender that is not valid for this application. The sender must be the email address of a registered administrator for the application, or the address of the current signed-in user. Administrators can be added to an application using the Administration Console. The current user's email address can be determined with the Users API.

exception InvalidEmailError()
An email address is not valid. Only valid email addresses are allowed for email address fields, such as sender or to.

exception InvalidAttachmentTypeError()
The EmailMessage has at least one attachment with a filename extension that is not allowed. See the Mail Service overview for a list of allowed filename extensions.

exception InvalidHeaderNameError()
The EmailMessage has at least one header that is not allowed. See the Mail Service Overview for a list of allowed headers.

exception MissingRecipientsError()
The EmailMessage does not have recipients set in any of these fields: to, cc, bcc. At least one recipient must be set to send the message.

exception MissingSenderError()
The EmailMessage is missing a value for the sender field. The sender must be set to send the message.

exception MissingSubjectError()
The EmailMessage is missing a value for the subject field. The subject must be set to send the message.

exception MissingBodyError()
The EmailMessage is missing a value for the body field. The body must be set to send the message.
        """


class Test(RequestHandler):
    def get(self):
        #from pygments import highlight
        #from pygments.formatters import HtmlFormatter
        #from pygments.lexers.json import JSONLexer
        self.response.out.write("""<html><head><link href="/gbkit/asset/stylesheets/pygments.css" rel="stylesheet" media="all" /></head><body>""")
        self.response.out.write('</pre></body></html>')


class NotFound(RequestHandler):
    def get(self):
        self.raise_error(code=404)


routes = [
    (r'/.*/tests/template-context', DumpTemplateContext),
    (r'/.*/tests/template', TestTemplateEngine),
    (r'/.*/tests/code-(.*)', TestCode),
    (r'/.*/tests/exception-timeout', TestTimeout),
    (r'/.*/tests/dev', Test),
    (r'/.*/tests/index', Index),
    (r'/.*/tests/', Index),
    (r'.*', NotFound)
]
app = WSGIApplication(routes, debug=DEBUG, config=wsgi_config)
