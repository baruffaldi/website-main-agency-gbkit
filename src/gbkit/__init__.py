#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# GBKit
# - WebApps Python Handy Framework
#
# Copyright 2012 GB Consulting
# Author: Filippo Baruffaldi <filippo@baruffaldi.info>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: GB Consulting
@project: GBKit
@license: Apache 2.0
"""
__author__ = "Filippo Baruffaldi <filippo@baruffaldi.info>"
__organization__ = "GB Consulting"
__project__ = "GBKit"
__license__ = "Apache 2.0"
__url__ = "http://www.gbc-italy.com"
__version__ = 1


def dump(var):
    import pprint
    import cStringIO as StringIO
    cb = StringIO.StringIO()
    pprint.pprint(var, cb)
    return cb.read()


def empty_function(self, *args, **kwargs):
    pass


class CsrfException(Exception):
    pass


class GBKitNotFoundException(Exception):
    code = 0
    text = "The requested content is unavailable"

    def __init__(self, code=0, text=None):
        if code:
            self.code = code
        if text:
            self.text = text


def LoggerDisabledDecorator(original_class):
    original_class.log = empty_function
    original_class.d_log = empty_function
    original_class.i_log = empty_function
    return original_class


class LoggerDisabledMetaClass(type):
    def __new__(cls, name, bases, attrs):
        attrs['log'] = empty_function
        attrs['d_log'] = empty_function
        attrs['i_log'] = empty_function
        return super(LoggerDisabledMetaClass, cls).__new__(
            cls, name, bases, attrs)


def ContentNotFound(request, response, exception):
    import logging
    logging.exception(exception)
    response.write('Oops! I could swear this page was here!')
    response.set_status(404)


def ContentNotWorking(request, response, exception):
    import logging
    logging.exception(exception)
    response.write('Oops! I could swear this page was working!')
    response.set_status(404)


def ContentProhibited(request, response, exception):
    import logging
    logging.exception(exception)
    response.write('Oops! I could swear this page was accessible and you was really beauty. But right now you can no more access it!')
    response.set_status(404)
